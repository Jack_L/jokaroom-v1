import { Component, OnInit, Input, SimpleChange,EventEmitter, Output, HostListener } from '@angular/core';
import { AjaxService } from '../../services/ajax.service';
import { Router } from '@angular/router';
import { Utility } from "../../utility/utility";
import { environment } from '../../../environments/environment';
import * as $ from 'jquery';
import * as _ from 'underscore';

@Component({
  selector: 'app-games-section-content',
  templateUrl: './games-section-content.component.html',
  styleUrls: ['./games-section-content.component.scss']
})
export class GamesSectionContentComponent implements OnInit {
  @Input() selectedGames:any;
  @Input() category:any;
  @Input() allFavGames;
  @Output() toggleFavourite = new EventEmitter<any>();
  @Output() playGame = new EventEmitter<string>();
  @Input() sortBy;
  windowType;
  @Input() listType;
  hostName;
  websiteCode
  constructor(
    private utils:Utility,
    private router:Router
  ) {}

  ngOnInit() {
    this.hostName = environment.siteUrl;
    this.websiteCode = environment.websiteCode;
  }

  @HostListener('window:resize') onResize() {
    // guard against resize before view is rendered
    this.getWindowType();
  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange }): void {
      var self = this;
    changes['selectedGames'] && changes['selectedGames'].currentValue ? this.selectedGames = changes['selectedGames'].currentValue : '';
    changes['category'] ? this.category = changes['category'].currentValue : '';
    changes['sortBy'] ? this.sortBy = changes['sortBy'].currentValue : '';
    changes['listType'] ? this.listType = changes['listType'].currentValue : '';
    if(this.selectedGames && this.selectedGames.length > 0 && $('.game-searchbar').hasClass('fix-search')){
      setTimeout(function(){
        self.utils.setContainerSideNav();
      })
    }
    //this.setHoverEffect();
  }

  ngAfterViewInit(){
    // $(".games-row").on('mouseenter mouseleave','.game-card__content',function() {
    //   $(this).children(".game-card__action").children(".game-card__action-btn").toggle();
    // });
    this.getWindowType();
  }

  getWindowType(){
    var ww = document.body.clientWidth;
    if(ww <= 991){
      this.windowType = "mobile";
    }else{
      this.windowType = "desktop";
    }
  }

  openGame(gameId){
    this.playGame.emit(gameId);
  }

  toggleFavouriteForGame(pristineId,gameCode){
    if(this.utils.isUserLoggedIn()){
        this.toggleFavourite.emit({"gameID":gameCode,"newState":this.allFavGames[pristineId] ? false:true});
    }
  }

  showProviderName(game,index):boolean{
    let showGroup = true;

    if(index > 0 && game[index].vendorName == game[index-1].vendorName){
      showGroup = false;
    }
    return showGroup;
  }

  doNothing(){}

}
