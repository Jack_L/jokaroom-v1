import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesSectionHeaderComponent } from './games-section-header.component';

describe('GamesSectionHeaderComponent', () => {
  let component: GamesSectionHeaderComponent;
  let fixture: ComponentFixture<GamesSectionHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamesSectionHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesSectionHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
