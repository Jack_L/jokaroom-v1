import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, SimpleChange } from '@angular/core';
import { gameCategories } from '../config/game-config';
import { Utility } from '../../utility/utility';
import * as $ from 'jquery';
import 'bootstrap-select';

@Component({
  selector: 'app-games-section-header',
  templateUrl: './games-section-header.component.html',
  styleUrls: ['./games-section-header.component.scss'],
    encapsulation: ViewEncapsulation.None

})
export class GamesSectionHeaderComponent implements OnInit {
  @Input() sortBy;
  @Input() selectedCategory;
  @Output() sortGames = new EventEmitter<String>();
  @Output() searchGames = new EventEmitter<String>();
  @Output() filterCategory = new EventEmitter<String>();
  @Input() listType;
  @Output() changeDisplayType = new EventEmitter<String>();
  availableCategories;
  constructor(
    private utils:Utility
  ) {}

  ngOnInit() {
    var self = this;
    this.availableCategories = gameCategories;
    $(window).scroll(function() {
      self.utils.gameHeaderFixer();
    });
  }

    ngOnChanges(changes: {
      [propName: string]: SimpleChange
    }): void {
      this.selectedCategory = changes['selectedCategory'].currentValue
      if(this.selectedCategory){
        $("#gameSearchText").val("");
      }
    }

  sortGamesOn(sortType){
    this.sortGames.emit(sortType);
  }

  findGame(searchValue){
    this.searchGames.emit(searchValue);
  }
  ngAfterViewInit(){
    var self = this;
    setTimeout(function(){
      $('select').selectpicker({}).on('change', function(){
        self.filterCategory.emit($('#filter-gamemenu option:selected').val());
      });
    },1)

  }

  changeListType(displayType){
    this.changeDisplayType.emit(displayType);
  }
}
