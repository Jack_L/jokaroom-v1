import { Component, OnInit } from '@angular/core';
import { AjaxService } from '../../services/ajax.service';
import { EmitterService } from '../../services/emitter.service';
import { GameService } from '../../services/game.service';
import { Utility } from "../../utility/utility";
import { environment } from '../../../environments/environment';
import { providerGameSortOrder } from '../../utility/config';

import * as $ from 'jquery';
import * as _ from 'underscore';

@Component({
  selector: 'app-games-section',
  templateUrl: './games-section.component.html',
  styleUrls: ['./games-section.component.scss']
})
export class GamesSectionComponent implements OnInit {
  gameRawData;
  sortedGameData;
  indexedGameData;
  selectedGames;
  allFavGames;
  category;
  sortBy;
  favUpdateSubs;
  selectedGame;
  listType = "grid";
  hostName;
  websiteCode;
  isUserLoggedIn;
  loginSubs;
  logoutSubs;
  openGameSubs;
  showFavSubs;
  oldCategory;
  constructor(
    private ajaxService:AjaxService,
    private gameService:GameService,
    private emitterService:EmitterService,
    private utils:Utility
  ) {
    this.favUpdateSubs = this.emitterService.favGameUpdate$.subscribe(
      favGameStatus => {
        if(favGameStatus == "UPDATED"){
          this.setFavGame();
        }
      }
    )

    this.openGameSubs = this.emitterService.openGamePopUp$.subscribe(
      openGamePopUpStatus => {
        if(openGamePopUpStatus == "openGamePopup"){
          var self = this;
          var ww = document.body.clientWidth;
          if(ww <= 991){
            setTimeout(function(){
                self.utils.openModal("mobileGameClickModal",false);
            },1)
          }

        }
      }
    )

    this.loginSubs = this.emitterService.loginComplete$.subscribe(
      loginStatus => {
        if (loginStatus == "LoginCompleted") {
          this.isUserLoggedIn = this.utils.isUserLoggedIn();
          this.getGames();
        }
      }
    )

    this.logoutSubs = this.emitterService.logoutComplete$.subscribe(
      logoutStatus => {
        if (logoutStatus == "LogoutCompleted") {
          this.isUserLoggedIn = this.utils.isUserLoggedIn();
        }
      }
    )

    this.showFavSubs = this.emitterService.showFavouriteGames$.subscribe(
      showFavStatus => {
        if (showFavStatus == "showFavourites") {
          this.getGamesOnCategory("favourite",false)
        }
      }
    )


  }

  ngOnInit() {
    this.hostName = environment.siteUrl;
    this.websiteCode = environment.websiteCode;
    this.isUserLoggedIn = this.utils.isUserLoggedIn();
    this.sortBy = 'displayOrder';
    this.getGames();
  }

  getGames(){
    let self = this;
    Promise.resolve(this.gameService.getGameAllData())
    .then(
      gameAllData =>{
        window["prerenderReady"] = false;
        this.sortedGameData = _.sortBy(gameAllData["rawGameData"],function(game){
              return game[self.sortBy];
        });
        this.allFavGames = gameAllData["allFavGames"];
        this.indexedGameData = gameAllData["indexedGameData"];
        this.getGamesOnCategory("top20",true);
      }
    );
  }

  getGamesOnCategory(categoryName,initial){
    var self = this;
    this.selectedGames = undefined;
    this.category = categoryName;
    let selectedGames;
    switch(categoryName){
      case 'top20':
        selectedGames = this.sortedGameData.length > 0 ? this.sortedGameData.slice(0,30) : [];
      break;
      case 'videoslots':
        selectedGames = _.filter(this.sortedGameData,function(game){
          return game.gameType == "video_slots"
        });
      break;
      case 'newGames':
        selectedGames = _.filter(this.sortedGameData,function(game){
          return game.isNew
        });
      break;
      case 'classicSlots':
        selectedGames = _.filter(this.sortedGameData,function(game){
          return game.gameType == "classic_slots"
        });
      break;
      case 'blackJack':
        selectedGames = _.filter(this.sortedGameData,function(game){
          return game.gameType == "blackjack"
        });
      break;
      case 'roulette':
        selectedGames = _.filter(this.sortedGameData,function(game){
          return game.gameType == "roulette"
        });
      break;
      case 'tableGames':
        selectedGames = _.filter(this.sortedGameData,function(game){
          return game.gameType == "table_games"
        });
      break;
      case 'videopoker':
        selectedGames = _.filter(this.sortedGameData,function(game){
          return game.gameType == "video_poker"
        });
      break;
      case 'jackpot':
        selectedGames = _.filter(this.sortedGameData,function(game){
          return game.gameType == "jackpot"
        });
      break;
      case 'other':
        selectedGames = _.filter(this.sortedGameData,function(game){
          return game.gameType == "others"
        });
      break;
      case '3dslots':
        selectedGames = _.filter(this.sortedGameData,function(game){
          return game.gameType == "3d_slots"
        });
      break;
      case 'allgames':
        selectedGames = this.sortedGameData;
      break;
      case 'favourite':
        this.selectedGames = undefined;
        this.getfavouriteGames();
      break;
      default:
        selectedGames = this.sortedGameData.length > 0 ? this.sortedGameData.slice(0,20) : [];
      break;
    }
    this.sortGames(this.sortBy,selectedGames,initial)
  }

  sortGames(sortType,selectedGames,initial){
    let self = this;
    self.sortBy = sortType;
    if(sortType != "vendorName" || (sortType == "vendorName" && (!providerGameSortOrder || (providerGameSortOrder && providerGameSortOrder.length <=0)))){
      this.selectedGames = _.sortBy(selectedGames,function(game){
        return game[self.sortBy];
      })
    }else{
      let providerIndexedGame = _.groupBy(selectedGames,'vendorName');
      this.selectedGames = []
      _.each(providerGameSortOrder,function(vendorName){
        if(providerIndexedGame[vendorName]){
          self.selectedGames = self.selectedGames.concat(providerIndexedGame[vendorName]);
          delete providerIndexedGame[vendorName];
        }
      })
      if(!_.isEmpty(providerIndexedGame)){
        _.each(providerIndexedGame,function(games){
          self.selectedGames = self.selectedGames.concat(games);
        })
      }
    }
    if(!initial){
      self.scrollAndFixContentAndHeader()
    }
  }

  searchGames(searchValue){
    if(this.sortedGameData.length > 0 && searchValue.length > 0){
      if(this.category){
        this.oldCategory = this.category;
        this.category = undefined;
      }
      this.selectedGames = _.filter(this.sortedGameData, function(game){
        return game['name'].toLowerCase().indexOf(searchValue.toLowerCase()) != -1
      });
    }else{
      this.getGamesOnCategory(this.oldCategory,false);
    }
    this.scrollAndFixContentAndHeader()
  }

  scrollAndFixContentAndHeader(){
    var self = this;
    $("body,html").animate({
      scrollTop: $(".cm-banner").offset().top + 230
    });
    setTimeout(function(){
      self.utils.gameContentFixer()
      self.utils.gameHeaderFixer();
    })
  }

  getfavouriteGames(){
    let self = this;
    Promise.resolve(this.gameService.getFavouriteGames())
    .then(
      favGameData=>{
        this.allFavGames  = this.gameService.formFavGameData(favGameData);
        this.sortGames(this.sortBy,_.toArray(this.allFavGames),false)
        //this.selectedGames = _.toArray(this.allFavGames);
      }
    )
  }

  setFavGame(){
    this.allFavGames = this.gameService.getAvailableFavGames();
  }

  toggleFavourite(gameFavData){
    Promise.resolve(this.gameService.toggleFavouriteGame(gameFavData))
    .then(
      favGameData => {
        this.allFavGames  = this.gameService.formFavGameData(favGameData);
      }
    )
  }

  toggleFavouriteForGame(pristineId,gameCode){
    if(this.utils.isUserLoggedIn()){
        this.toggleFavourite({"gameID":gameCode,"newState":this.allFavGames[pristineId] ? false:true});
    }
  }

  changeDisplayType(displayType){
    this.listType = displayType;
  }

  playGame(gameId){
    var self = this;
    var ww = document.body.clientWidth;
    if(ww <= 991){
      this.selectedGame = this.indexedGameData[gameId];
      setTimeout(function(){
          self.utils.openModal("mobileGameClickModal",false);
      },1)
    }else{
      this.gameService.goToGame(this.indexedGameData[gameId].gameCode,'');
    }
  }

  openGame(gameId,force){
    this.utils.closeAllOpenModel();
    this.gameService.goToGame(this.indexedGameData[gameId].gameCode,force);
  }


  openModalFromGame(modalId,isTrue){
    this.utils.closeAllOpenModel();
    this.gameService.isCalledOnGameOpen = true;
    if(modalId == "signUpModal-"){
      this.emitterService.broadcastOpenRegistration("Open");
    }else{
      this.utils.openModal(modalId,isTrue);
    }
  }

  ngOnDestroy(){
      this.favUpdateSubs.unsubscribe();
      this.loginSubs.unsubscribe();
      this.logoutSubs.unsubscribe();
      this.showFavSubs.unsubscribe();
      $("body>#mobileGameClickModal").remove();
  }

}
