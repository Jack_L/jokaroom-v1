import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesFooterComponent } from './games-footer.component';

describe('GamesFooterComponent', () => {
  let component: GamesFooterComponent;
  let fixture: ComponentFixture<GamesFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamesFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
