import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasinoGamesComponent } from './casino-games.component';
import { GamesSectionComponent } from './games-section/games-section.component';
import { GamesFooterComponent } from './games-footer/games-footer.component';
import { GamesSectionHeaderComponent } from './games-section-header/games-section-header.component';
import { GamesSectionSideNavComponent } from './games-section-side-nav/games-section-side-nav.component';
import { GamesSectionContentComponent } from './games-section-content/games-section-content.component';
import { SharedModule } from '../shared-modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule.forRoot()
  ],
  declarations: [
    CasinoGamesComponent,
    GamesSectionComponent,
    GamesFooterComponent,
    GamesSectionHeaderComponent,
    GamesSectionSideNavComponent,
    GamesSectionContentComponent
  ],
  exports: [CasinoGamesComponent]
})
export class CasinoGamesModule { }
