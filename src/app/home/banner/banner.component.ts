import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BannerService } from '../../services/banner.service';
import { Utility } from '../../utility/utility';
import { environment } from '../../../environments/environment';
import { EmitterService } from '../../services/emitter.service';

import * as _ from 'underscore';
declare var $:any;

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
  encapsulation : ViewEncapsulation.None
})
export class BannerComponent implements OnInit {
  banners;
  loginSubs;
  logoutSubs;
  @Input() zoneId;
  idArray =  [];
  cashierTransactionSuccess;
  constructor(
    private bannerService:BannerService,
    private utils:Utility,
    private emitterService:EmitterService,
    private router:Router,
  ) {

    this.loginSubs = this.emitterService.loginComplete$.subscribe(
      loginStatus => {
        if (loginStatus == "LoginCompleted") {
          this.getBanners(true);
        }
      }
    )

    this.logoutSubs = this.emitterService.logoutComplete$.subscribe(
      logoutStatus => {
        if (logoutStatus == "LogoutCompleted") {
          this.getBanners(true);
        }
      }
    )

    this.cashierTransactionSuccess = this.emitterService.getBannerAndPromo$.subscribe(
      getBannerAndPromoData => {
        if (getBannerAndPromoData == "TimeOutOver") {
          var self = this;
          self.getBanners(true);
        }
      }
    )

  }

  ngOnInit() {
    this.getBanners(false);
  }

  getBanners(isForce){
    $('.carousel').carousel({
      interval: false
    });
    this.banners = undefined;

    Promise.resolve(this.bannerService.getBanners(this.getDataToSend(),isForce))
    .then(
      banners=>{
          this.setBanner(banners);
      }
    )
  }

  setBanner(banners){
    var self = this;
    let bannersTest = banners;
    this.banners = []
    if(this.zoneId == '8' && bannersTest.length > 0){
      this.banners.push(bannersTest[0]);
    }else{
      this.banners = bannersTest;
    }
    setTimeout(function(){
      _.each(self.banners,function(banner){
        $("#"+banner.id).html(banner.content);
        $("#"+banner.id+" .img-holder .small-screen-banner").attr("src",environment.siteUrl+'/uploads/banner/'+banner.mobileImage);
        $("#"+banner.id+" .img-holder .large-screen-banner").attr("src",environment.siteUrl+'/uploads/banner/'+banner.imagePath);
        if(banner.button){
          $("#"+banner.id+" .cm-slider__btn span").html(banner.button);
        }
      })

      self.utils.elementsChange();

      $("#cmSlider").on('click',function(e){
        let routerLink = $(e.target).attr("routerlink");
        if(routerLink){
          self.utils.cmsRouter(routerLink);
        }
      })
      $('.carousel').carousel({
        interval: 1000 * 5
      });

    })
  }

  getDataToSend(){
    let bannerData ={'zoneId':this.zoneId ? this.zoneId : '1'}
    let affId = this.utils.getAffIdCookie("affIdCookie");
    if(affId){
      bannerData["affiliateId"] = affId;
    }
    return bannerData;
  }

  ngOnDestroy(){
    this.loginSubs.unsubscribe();
    this.logoutSubs.unsubscribe();
    this.cashierTransactionSuccess.unsubscribe();
  }

}
