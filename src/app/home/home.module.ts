import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannerComponent } from './banner/banner.component';
import { HomeComponent } from './home.component';
import { CasinoGamesModule } from '../casino-games/casino-games.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared-modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    CasinoGamesModule,
    RouterModule,
    SharedModule.forRoot()
  ],
  declarations: [
    HomeComponent,
    BannerComponent
  ],
  exports: [
    HomeComponent,
    BannerComponent
  ]
})
export class HomeModule { }
