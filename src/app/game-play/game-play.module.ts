import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GamePlayComponent } from './game-play.component';
import { GameRouteModule } from './game-route.module';
import { HomeModule } from '../home/home.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared-modules/shared.module';


@NgModule({
  imports: [
    CommonModule,
    GameRouteModule,
    HomeModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule.forRoot()
  ],
  declarations: [GamePlayComponent],
  exports: [GamePlayComponent]
})
export class GamePlayModule { }
