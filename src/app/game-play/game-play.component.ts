import { Component, OnInit, ViewEncapsulation, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { AjaxService } from '../services/ajax.service';
import { EmitterService } from '../services/emitter.service';
import { GameService } from '../services/game.service';
import { UserDetailsService } from '../services/user-details.service';
import { Utility } from '../utility/utility';
import { currencyCodeMap } from '../utility/config';
import { SocketService } from '../shared-modules/services/socket.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';

import * as $ from 'jquery';
import * as _ from 'underscore';
// import 'bigslide';


@Component({
  selector: 'app-game-play',
  templateUrl: './game-play.component.html',
  styleUrls: ['./game-play.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class GamePlayComponent implements OnInit {
  gameId;
  gameType;
  selectedGame;
  isFavourite;
  favUpdateSubs;
  loginSubs;
  logoutSubs;
  isUserLoggedIn;
  statusCallDoneSubs;
  winEventSubs;
  winnersList = [];
  gameTypeIdGameMap = {};
  gameUrl;
  gameTypesSubs;
  constructor(
    private activatedRoute: ActivatedRoute,
    private ajaxService: AjaxService,
    private router: Router,
    private gameService: GameService,
    private emitterService: EmitterService,
    private utils: Utility,
    private socketService:SocketService,
    private userDetailsService:UserDetailsService
  ) {
    this.favUpdateSubs = this.emitterService.favGameUpdate$.subscribe(
      favGameStatus => {
        if (favGameStatus == "UPDATED") {
          this.setFavGame();
        }
      }
    )

    this.loginSubs = this.emitterService.loginComplete$.subscribe(
      loginStatus => {
        if (loginStatus == "LoginCompleted") {
          this.isUserLoggedIn = this.utils.isUserLoggedIn();
          this.gameType = 'getRealGames';
          this.getGamePlayUrl(this.gameType);
        }
      }
    )

    this.logoutSubs = this.emitterService.logoutComplete$.subscribe(
      logoutStatus => {
        if (logoutStatus == "LogoutCompleted") {
          this.isUserLoggedIn = this.utils.isUserLoggedIn();
          this.gameType = 'getFreeGames';
          this.getGamePlayUrl(this.gameType);
        }
      }
    )

    this.statusCallDoneSubs = this.emitterService.statusCallDone$.subscribe(
      statusCallDoneData => {
        if (statusCallDoneData == "statusCallSuuccess") {
          this.socketService.connectToScoket("winEvent");
        }
      }
    )

    this.winEventSubs = this.emitterService.winEvent$.subscribe(
      winEventData => {
        if (winEventData) {
          this.addWinnerToList(winEventData);
        }
      }
    )

    this.gameTypesSubs = this.emitterService.getGametypes$.subscribe(
      getGametypesData => {
        if (getGametypesData) {
          this.getGamePlayUrl(getGametypesData);
        }
        if(getGametypesData == "getFreeGames"){
          $(".get-free-games").addClass("hide");
          $(".get-real-games").removeClass("hide");
        }else {
          $(".get-free-games").removeClass("hide");
          $(".get-real-games").addClass("hide");
        }
      }
    )

  }

  @HostListener('window:resize') onResize() {
    // guard against resize before view is rendered
    this.setGameHeight();
  }

  ngOnInit() {
    var self = this;
    this.socketService.connectToScoket("winEvent");
    this.isUserLoggedIn = this.utils.isUserLoggedIn();
    this.activatedRoute.params.subscribe(params => {
      this.gameId = params['gameId'];
      this.gameType = params['gameType'] == "gamelauncher" ? 'getRealGames' : 'getFreeGames';
      if (this.utils.isUserLoggedIn()) {
        this.gameType = 'getRealGames';
      } else {
        this.gameType = 'getFreeGames';
      }

      if(this.gameService.isForcePlay){
        this.gameType = this.gameService.isForcePlay;
        this.gameService.isForcePlay = "";
      }

      Promise.resolve(this.ajaxService.getWinners())
      .then(
        gameWinners => {
          if(gameWinners && gameWinners["winners"]){
            if(gameWinners["winners"]["winnersListDetails"] && gameWinners["winners"]["winnersListDetails"].length > 0){
              this.winnersList = gameWinners["winners"]["winnersListDetails"];
            }else{
              this.winnersList = []
            }
            if(gameWinners["winners"]["gameDataList"] && gameWinners["winners"]["gameDataList"]){
              this.gameTypeIdGameMap = gameWinners["winners"]["gameDataList"];
            }else{
              this.gameTypeIdGameMap = {}
            }
          }else{
            this.winnersList = []
            this.gameTypeIdGameMap = {}
          }
        }
      );

      window["prerenderReady"] = false;
      Promise.resolve(this.gameService.getGameAllData())
      .then(
        gameAllData => {
          this.selectedGame = _.filter(gameAllData["rawGameData"], function(game) {
            return game.gameCode == self.gameId
          })[0];
          self.utils.setSEO(this.selectedGame,true);
          window["prerenderReady"] = true;
          if(gameAllData["allFavGames"] && !_.isEmpty(gameAllData["allFavGames"])){
            this.isFavourite = gameAllData["allFavGames"][this.selectedGame.pristineGameId];
          }
          this.gameService.setLastPlayed(this.selectedGame.pristineGameId);
        }
      );
      this.getGamePlayUrl(this.gameType);
    });
  }

  addWinnerToList(newWinner){
    newWinner["gameName"] = this.gameTypeIdGameMap[newWinner.gameTypeId]
    newWinner["amountInHouseCurrency"] = currencyCodeMap[newWinner["currency"]]+newWinner["amountInHouseCurrency"]
    this.winnersList.unshift(newWinner);
  }

  toggleFavouriteGame(currentState, gameCode) {
    let gameFavData;
    Promise.resolve(this.gameService.toggleFavouriteGame({ "gameID": gameCode, "newState": !currentState }))
    .then(
      favGameData => {
        let allFavGames = this.gameService.formFavGameData(favGameData);
        this.isFavourite = allFavGames[this.selectedGame.pristineGameId];
      }
    )
  }

  setFavGame() {
    this.isFavourite = this.selectedGame ? this.gameService.getAvailableFavGames()[this.selectedGame.pristineGameId] : '';
  }

  getGamePlayUrl(gameType) {
    var self = this;
    let gamePlayData = {
      gameSymbol: this.gameId
    }
    this.gameUrl = undefined;
    $("#gameIframeWrapper").empty();
    Promise.resolve(this.ajaxService.getGamePlayUrl(gameType, gamePlayData))
    .then(resp => {
      this.gameUrl = true;
      if(gameType == "getFreeGames"){
        $('#game-display__sidebar, #game-display__container, #game-display__sidebar-open-btn').addClass('active');
      }
      if(resp && resp["gameDetails"] && resp["gameDetails"]["url"]){
        this.openIframe(resp["gameDetails"]["url"]);
      }else if(resp && resp["gameDetails"] && resp["gameDetails"]["error"]){
        let errorMessage = "";
        _.each(resp["gameDetails"]["error"],function(value,key){
          errorMessage += value + " "
        })
        setTimeout(function(){
          $("#gameIframeWrapper").append("<h1 style='color:white'>" + errorMessage + "</h1>");
        })
      }else{
        setTimeout(function(){
          $("#gameIframeWrapper").append("<h1 style='color:white'>Something went wrong. Please try again later.</h1>");
        })
      }
      this.gameType = gameType;
      setTimeout(function(){
        self.setGameHeight();
      })
    })
  }

  openIframe(url){
    var ww = document.body.clientWidth;
    let windowType;
    var ua = navigator.userAgent,
      isMobileWebkit = /WebKit/.test(ua) && /Mobile/.test(ua);
    if(ww <= 991 || isMobileWebkit){
      window.location.href = url;
    }else{
      this.gameUrl = url;
      let $iframe = $('<iframe name="gameIframe" id="gameIframe" src="' + url + '" width="640" height="480"></iframe>');
      setTimeout(function(){
        $("#gameIframeWrapper").append($iframe);
      })
    }

  }

  ngAfterViewInit() {
    var self = this;
    // $(document).ready(function() {
    //   $('.menu-link').bigSlide({
    //     menuWidth: '300px',
    //     push: ('.push'),
    //     speed: '450',
    //     easyClose: 'false'
    //   });


    // });

    // global vars
    var winWidth = $(window).width();
    var winHeight = $(window).height();

    // set initial div height / width
    // $('.game-display__frame--inner iframe').css({
    //   'width': 'auto',
    //   'height': winHeight,
    // });

    // make sure div stays full width/height on resize
    // $(window).resize(function() {
    //   $('.game-display__frame--inner iframe').css({
    //     'width': 'auto',
    //     'height': winHeight,
    //   });
    // });

    $('#game-display__sidebar-open-btn').on('click', function() {
      $('#game-display__sidebar, #game-display__container, #game-display__sidebar-open-btn').toggleClass('active');
      self.setGameHeight();
    });



  }

  setGameHeight() {
    var ww = document.body.clientWidth;
    if (ww <= 991) {
      $("#game-display__frame").css("width", "auto")
      $("#gameIframe, #gameIframeWrapper, #game-display__frame").css("height", "")
    } else {
      let windowHeight = $(window).height();
      let fixedFooterHeight = $(".fixed-footer").height();

      let windowWidth = $(window).width();
      if($("#game-display__sidebar").hasClass('active')){
        windowWidth -= $("#game-display__sidebar").width();
      }

      if( (windowHeight - fixedFooterHeight ) * 2 > windowWidth){
        $("#game-display__frame").css("width", (windowWidth-50)  + "px")
        $("#gameIframe, #gameIframeWrapper, #game-display__frame").css("height", (windowWidth)/2 + "px")
      }else{
        $("#game-display__frame").css("width", (windowHeight - fixedFooterHeight - 50) * 2 + "px")
        $("#gameIframe, #gameIframeWrapper, #game-display__frame").css("height", (windowHeight - fixedFooterHeight) + "px")
      }


    }

    $(window).resize(function() {
      $('#game-display__frame').css({
        'width': 'auto',
        'height': '$("#gameIframe").height()'
      });
    });


  }

  closeGame() {
    this.router.navigate(["/en"]);
  }


  toggleFullScreen() {

    var iframe = document.querySelector('#gameIframeWrapper iframe');;
    if(iframe){
      if (iframe["requestFullscreen"]) {
        iframe["requestFullscreen"]();
      } else if (iframe["webkitRequestFullscreen"]) {
        iframe["webkitRequestFullscreen"]();
      } else if (iframe["mozRequestFullScreen"]) {
        iframe["mozRequestFullScreen"]();
      } else if (iframe["msRequestFullscreen"]) {
        iframe["msRequestFullscreen"]();
      }

    }




  }

  ngOnDestroy() {
    this.userDetailsService.forceGetUserProfileDetails();
    this.favUpdateSubs.unsubscribe();
    this.loginSubs.unsubscribe();
    this.logoutSubs.unsubscribe();
    this.statusCallDoneSubs.unsubscribe();
    this.winEventSubs.unsubscribe();
    this.utils.setSEO("","");
  }

}
