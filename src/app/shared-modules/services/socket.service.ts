import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import * as socketIo from 'socket.io-client';
import { Socket } from '../socket-interface/socket.interface';
import { UserDetailsService } from '../../services/user-details.service';
import { AjaxService } from '../../services/ajax.service';
import { EmitterService } from '../../services/emitter.service';



@Injectable()
export class SocketService {

  socket: Socket;
  observer: Observer<any>;
  socketUrl:string;
  partnerId:string;
  sessionId:string;
  isSessionSocketCreated:boolean = false;
  isPartnerSocketCreated:boolean = false;
  isForceDisconnect = false;
  logoutSubs;
  interval;
  constructor(
    private userDetailsService:UserDetailsService,
    private ajaxService:AjaxService,
    private emitterService:EmitterService
  ){
    this.logoutSubs = this.emitterService.logoutComplete$.subscribe(
      logoutStatus => {
        if (logoutStatus == "LogoutCompleted") {
          this.sessionId = undefined;
          this.isSessionSocketCreated = false;
          this.isPartnerSocketCreated = false;
          this.socketUrl = undefined;
          if(this.socket) this.socket.disconnect();
          this.socket = undefined;
        }
      }
    )
  }

  setSocketUrl(socketUrl){
    this.socketUrl = socketUrl;
  }

  setSessionId(sessionId){
    this.sessionId = sessionId;
  }

  setPartnerId(partnerId){
    this.partnerId = partnerId;
  }



  connectToScoket(eventType){
    if(this.socketUrl){
      var self= this;
      if(!self.socket || eventType =="onDisconnect"){
        self.socket = socketIo(self.socketUrl);
      }else{
        if(!self.socket["connected"]){
          self.socket.connect();
        }
      }

      self.socket.on('connect',function(esocket){
        if(self.isForceDisconnect){
          self.isForceDisconnect = false;
          self.socket["disconnect"]();
        }else{
          self.onConnectActions();
        }

        self.socket.on('disconnect',function(){
          self.socket["removeAllListeners"]("message");
          self.socket["removeAllListeners"]("winEventMessage");
          self.connectToScoket("onDisconnect");
        })


      });

    }
  }



  // connectToScoket(eventType) {
  //   if(this.socketUrl){
  //     var self= this;
  //     if((eventType == "authCode" && !self.isSessionSocketCreated) || (eventType == "winEvent" && !self.isPartnerSocketCreated)){
  //       self.isSessionSocketCreated = false;
  //       self.isPartnerSocketCreated = false;
  //       if(self.socket && self.socket["connected"]){
  //         self.socket["disconnect"]();
  //       }else if(self.socket && !self.socket["connected"]){
  //         self.isForceDisconnect = true;
  //       }else{
  //         self.socket = socketIo(self.socketUrl,{forceNew:true});
  //       }
  //     }
  //
  //     self.socket.on('connect',function(esocket){
  //       self.interval = setInterval(function(){
  //         self.socket.send('ping');
  //       },10000);
  //       if(self.isForceDisconnect){
  //         self.isForceDisconnect = false;
  //         self.socket["disconnect"]();
  //       }else{
  //         self.onConnectActions();
  //         self.socket.on('reconnect',function(esocket){
  //           self.isSessionSocketCreated = false;
  //           self.isPartnerSocketCreated = false
  //           self.onConnectActions();
  //         })
  //       }
  //
  //     });
  //
  //     self.socket.on('disconnect',function(){
  //       clearTimeout(self.interval);
  //       self.socket["removeAllListeners"]("setAuthCode");
  //       self.socket["removeAllListeners"]("listenWinEventMessage");
  //       this.isSessionSocketCreated = false;
  //       this.isPartnerSocketCreated = false
  //       if(self.socketUrl)
  //       self.socket = socketIo(self.socketUrl);
  //     })
  //
  //     self.socket.on('error', (error) => {
  //       clearTimeout(self.interval);
  //       self.socket["removeAllListeners"]("setAuthCode");
  //       self.socket["removeAllListeners"]("listenWinEventMessage");
  //       this.isSessionSocketCreated = false;
  //       this.isPartnerSocketCreated = false
  //       if(self.socketUrl)
  //       self.socket = socketIo(self.socketUrl);
  //     });
  //
  //   }
  // }


  onConnectActions(){
    var self = this;
    self.socket["removeAllListeners"]("message");
    self.socket["removeAllListeners"]("winEventMessage");
    if(self.sessionId){
      self.socket.emit("setAuthCode", self.sessionId);
    }
    if(self.partnerId){
      self.socket.emit("listenWinEventMessage", self.partnerId);
    }
    self.socket.on('winEventMessage', function(msg) {
      console.log('got win');
      let message = {
          "screenName": msg.playerName,
          "amountInHouseCurrency": msg.winAmountInInHouseCurrency,
          "currency": msg.ecrCurrency,
          "gameTypeId": msg.gameTypeId
      }
      self.emitterService.broadcastWinEvent(message);
    });
    self.socket.on('message', (data) => {
      console.log('update Balance');
      if(data['messageName'] && data['messageName'].includes("PushRealBalance")){
        if(self.userDetailsService.getUserBalanceDetails){
          let newBalanceDetails = self.userDetailsService.getUserBalanceDetails;
          newBalanceDetails["cash"] = Number(data["cashBalance"]);
          newBalanceDetails["bonus"] = Number(data["bonusBalance"]);
          self.userDetailsService.setUserCashBalance(Number(data["cashBalance"]));
          self.userDetailsService.setUserBonusBalance(Number(data["bonusBalance"]));
          self.userDetailsService.setUserTotalBalance(Number(data["cashBalance"]) + Number(data["bonusBalance"]));
          self.userDetailsService.setUserBalanceDetails(newBalanceDetails);
        }
      }else if(data['messageName'] && data['messageName'].includes("SessionTerminated")){

      }
    });
  }

}
