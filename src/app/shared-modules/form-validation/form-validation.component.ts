import { Component, OnInit } from '@angular/core';
import { Utility } from '../../utility/utility';

@Component({
  selector: 'app-form-validation',
  templateUrl: './form-validation.component.html',
  styleUrls: ['./form-validation.component.scss']
})
export class FormValidationComponent implements OnInit {

  focusedElement;
  private utility;
  constructor(
    utility:Utility
  ) {
    this.utility = utility;
  }

  ngOnInit() {
  }

  onFocusForElement(element){
    if(this.focusedElement != element){
      this.focusedElement = element
    }
  }

  onFocusOutForElement(){
    this.focusedElement = undefined;
  }

  onKeyPress(){
    this.focusedElement = undefined;
  }

  getFormFiledClass(fieldName,formName){
    if(this.focusedElement && this.focusedElement == fieldName){
      return '';
    }else{
      return this.shouldShowErrorsInverted(fieldName,formName) ? 'has-error' : this.isControlValid(fieldName,formName) ? 'has-success' : '';
    }
  }

  isControlValid(fieldName,formName){
    return this.utility.isControlValid(fieldName,formName)
  }

  shouldShowErrors(fieldName,formName){
    if(this.focusedElement && this.focusedElement == fieldName){
      return this.utility.shouldShowErrors(fieldName,formName)
    }else{
      return false;
    }
  }

  shouldShowErrorsInverted(fieldName,formName){
    if(this.focusedElement && this.focusedElement == fieldName){
      return false;
    }else{
      return this.utility.shouldShowErrors(fieldName,formName)
    }
  }

  getErrorMessage(fieldName,formName){
    return this.utility.getErrorMessage(fieldName,formName)
  }

  openModal(modalId,isForce) {
    this.utility.openModal(modalId,isForce)
  }

  closeModal(modalId,isSlideLeft,forceClose) {
    this.utility.closeModal(modalId,isSlideLeft,forceClose)
  }

  mobileNumberValidation(event: any) {

    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);

        if (!pattern.test(inputChar) && event.charCode != '0') {
            event.preventDefault();
        }
}

}
