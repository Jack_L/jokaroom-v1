import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-jkr-loader',
  templateUrl: './jkr-loader.component.html',
  styleUrls: ['./jkr-loader.component.scss'],
        encapsulation: ViewEncapsulation.None

})
export class JkrLoaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
