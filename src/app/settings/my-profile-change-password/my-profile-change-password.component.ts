import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormGroupDirective, NgForm, FormBuilder } from '@angular/forms';
import { CustomValidators } from '../../utility/custom-validator';
import { Utility } from '../../utility/utility';
import { AjaxService } from '../../services/ajax.service';
import { EmitterService } from '../../services/emitter.service';
import { FormValidationComponent } from '../../shared-modules/form-validation/form-validation.component';

@Component({
  selector: 'app-my-profile-change-password',
  templateUrl: './my-profile-change-password.component.html',
  styleUrls: ['./my-profile-change-password.component.scss']
})
export class MyProfileChangePasswordComponent extends FormValidationComponent implements OnInit {
  focusedElement;
  serverResponse;
  changePasswordForm = this.formBuilder.group({
    'old': ['', [this.CustomValidators.reqMin(4)]],
    'password': ['', [this.CustomValidators.reqMin(4)]],
    'confirmPassword': ['', [this.CustomValidators.reqMin(4)]],
	}, {validator:this.CustomValidators.passwordConfirming})
  constructor(
    private formBuilder:FormBuilder,
    private utils:Utility,
    private ajaxService: AjaxService,
    private CustomValidators:CustomValidators,
    private emitterService:EmitterService
  ) {
    super(utils);
  }

  ngOnInit() {
  }

  changePassword(buttonId){
    this.serverResponse = "";
    if(this.changePasswordForm.valid){
        this.utils.disableButton(buttonId,"",true);
	      let changePasswordData=this.utils.formControlToParams(this.changePasswordForm,{});
	      Promise.resolve(this.ajaxService.changePassword(changePasswordData))
	      .then(ChangePasswordResponse => {
	        if(ChangePasswordResponse && ChangePasswordResponse["success"] && ChangePasswordResponse["success"]["status"] === "SUCCESS"){
            this.serverResponse = "PASSWORD_CHANGE_SUCCESS";
            this.utils.enableButton(buttonId,"");
            var self = this;
            setTimeout(function(){
              self.emitterService.broadcastDoLogoutComplete("LOGOUT_USER_CLOSE_MODAL:settingsModal")
            },1000);
	        }else if(ChangePasswordResponse && ChangePasswordResponse["errors"] && ChangePasswordResponse["errors"]["message"]){
            this.serverResponse = ChangePasswordResponse["errors"]["message"];
            this.utils.enableButton(buttonId,"");
            if(ChangePasswordResponse["errors"]["old"]) this.changePasswordForm.controls["old"].setErrors({'message':ChangePasswordResponse["errors"]["old"].replace(/_/g," ")})
            if(ChangePasswordResponse["errors"]["password"]) this.changePasswordForm.controls["password"].setErrors({'message':ChangePasswordResponse["errors"]["password"]})
            if(ChangePasswordResponse["errors"]["confirmPassword"]) this.changePasswordForm.controls["confirmPassword"].setErrors({'message':ChangePasswordResponse["errors"]["confirmPassword"]})
          }else{
            console.log(ChangePasswordResponse);
            this.serverResponse = "Something went wrong. Please try again later";
            this.utils.enableButton(buttonId,"");
	        }
	      },
	      SystemError => {

	      });
    }else{
      this.utils.validateAllFormFields(this.changePasswordForm);
    }
  }

}
