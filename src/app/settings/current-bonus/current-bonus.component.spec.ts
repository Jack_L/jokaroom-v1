import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentBonusComponent } from './current-bonus.component';

describe('CurrentBonusComponent', () => {
  let component: CurrentBonusComponent;
  let fixture: ComponentFixture<CurrentBonusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentBonusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentBonusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
