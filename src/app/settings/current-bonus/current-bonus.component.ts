import { Component, OnInit, SimpleChange, Input,Output,EventEmitter } from '@angular/core';
import { AjaxService } from '../../services/ajax.service';
import { Utility } from '../../utility/utility';

declare var $:any;
@Component({
  selector: 'app-current-bonus',
  templateUrl: './current-bonus.component.html',
  styleUrls: ['./current-bonus.component.scss']
})
export class CurrentBonusComponent implements OnInit {

  @Input() activeBonusList;
  @Input() profileDetails;
  @Input() currencyDetails;
  @Output() getBonusDetails = new EventEmitter<String>();
  currencyCode;
  forefeitProgress = false;

  constructor(
    private ajaxService:AjaxService,
    private utils:Utility
  ) { }

  ngOnInit() {


  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange }): void {
    changes['activeBonusList'] && changes['activeBonusList'].currentValue ? this.activeBonusList = changes['activeBonusList'].currentValue : '';
    changes['profileDetails'] && changes['profileDetails'].currentValue ? this.profileDetails = changes['profileDetails'].currentValue : '';
    changes['currencyDetails'] && changes['currencyDetails'].currentValue ? this.currencyDetails = changes['currencyDetails'].currentValue : '';
    if(this.currencyDetails){
      this.currencyCode = this.currencyDetails.code;
    }
  }

  openForeFeit(index){
    if(!this.forefeitProgress){
      $(".popout").removeClass("show");
      $("#popout"+index).addClass("show");
    }
  }

  closeForeFeit(index){
    $("#popout"+index).removeClass("show");
  }

  forfeitBonus(bonusId){
    this.forefeitProgress = true;
    this.utils.disableButton("forfeitYes"+bonusId,"",true);
    this.utils.disableButton("forfeitNo"+bonusId,"",false);
    let bonusData = {
      "ecrBonusId" : bonusId
    }
    Promise.resolve(this.ajaxService.dropBonus(bonusData))
    .then(
      dropBonusData => {
        this.forefeitProgress = false;
        //bonusDropped: "SUCCESS", status: "SUCCESS", errorCode: null, errorDescription: null
        if(dropBonusData && dropBonusData["status"] && dropBonusData["status"] == "SUCCESS"){
          this.getBonusDetails.emit('active')
        }else{
          this.utils.enableButton("forfeitYes"+bonusId,"");
          this.utils.enableButton("forfeitNo"+bonusId,"");
        }
      }
    )
  }

}
