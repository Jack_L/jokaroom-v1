import { Component, OnInit, Input, SimpleChange, ViewEncapsulation} from '@angular/core';

import * as $ from 'jquery';
import 'intl-tel-input';
import 'bootstrap-select';


@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss'],
    encapsulation: ViewEncapsulation.None

})
export class MyProfileComponent implements OnInit {

  @Input() profileDetails;
  constructor() {}

  ngOnInit() {
    $('select').selectpicker({

    });

  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange
  }): void {
    changes['profileDetails'] ? this.profileDetails = changes['profileDetails'].currentValue : '';
  }

}
