import { Component, OnInit, Input, SimpleChange, Output,EventEmitter } from '@angular/core';
import { AjaxService } from '../../services/ajax.service';
import { Utility } from '../../utility/utility';
import * as $ from 'jquery';
import * as _ from 'underscore';
@Component({
  selector: 'app-kyc',
  templateUrl: './kyc.component.html',
  styleUrls: ['./kyc.component.scss']
})
export class KycComponent implements OnInit {
  @Output() proofSubmitted = new EventEmitter();
  @Input() kycDetails;
  sortedKycDetails;
  serverResponse;
  proofList={
    'identity': {'file': null, 'url': '/ajax/account/Documents/upload?file=nationalid_'},
    'address' : {'file': null, 'url': '/ajax/account/Documents/upload?file=utilitybill_'},
    'cc':{'file': null, 'url': '/ajax/account/Documents/upload?file=cc_'}
  }

  constructor(
    private ajaxService: AjaxService,
    private utils:Utility
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: {[propName: string]: SimpleChange}): void {
    changes['kycDetails'] && changes['kycDetails'].currentValue? this.kycDetails = changes['kycDetails'].currentValue: '';
    this.sortByKycStatus();
  }

  sortByKycStatus(){
    let kycDetailsObj;
    if(this.kycDetails){
      let idStatus = this.getDocumentStatus(this.kycDetails.identityStatus,"id");
      let addressStatus = this.getDocumentStatus(this.kycDetails.addressStatus,"address");
      let ccStatus = this.getDocumentStatus(this.kycDetails.ccStatus,"cc");
      let sortedKycTemp = [];
      sortedKycTemp.push(idStatus);
      sortedKycTemp.push(addressStatus);
      sortedKycTemp.push(ccStatus);
      this.sortedKycDetails = _.sortBy(sortedKycTemp, function(kycType){
        return kycType.forceSort;
      });
      // kycDetailsObj["identityStatus"]={
      //     status: this.kycDetails.identityStatus == "vrfn_new"
      // };
      // addressStatus
      // ccStatus
    }else{

    }
  }

  getDocumentStatus(type,docName):any{
    let detailsObject ={};
    switch(type){
      case "vrfn_new":
        detailsObject["status"] = "New";
        detailsObject["buttonText"] = "Upload File!";
        detailsObject["forceSort"] = 1;
      break;
      case "vrfn_init":
        detailsObject["status"] = "Processing";
        detailsObject["buttonText"] = "Upload Another File?";
        detailsObject["forceSort"] = 2;
      break;
      case "vrfn_verified":
        detailsObject["status"] = "Verified";
        detailsObject["buttonText"] = "Upload Another File?";
        detailsObject["forceSort"] = 3;
      break;
      case "vrfn_failed":
        detailsObject["status"] = "Rejected";
        detailsObject["buttonText"] = "Upload Another File?";
        detailsObject["forceSort"] = 3;
      break;
      default:
      detailsObject["status"] = "New";
      detailsObject["buttonText"] = "Upload File!";
      detailsObject["forceSort"] = 1;
      break;
    }

    switch(docName){
      case "id":
        detailsObject["desc"] = "Submit a copy of your ID card or Passport";
        detailsObject["type"] = "Photo ID";
        detailsObject["buttonId"] = "identityBillUpload";
        detailsObject["uploadType"] = "identity";
      break;
      case "address":
        detailsObject["desc"] = "Send us a copy of your last utility bill";
        detailsObject["type"] = "Proof of Address";
        detailsObject["buttonId"] = "utilityBillUpload";
        detailsObject["uploadType"] = "address";
      break;
      case "cc":
        detailsObject["desc"] = "Send us a copy of your registered Credit Card";
        detailsObject["type"] = "Payment Method";
        detailsObject["buttonId"] = "CCBillUpload";
        detailsObject["uploadType"] = "cc";
      break;
    }
    return detailsObject;
  }

  clearAlreadyAvailableFile(inputId){
    $("#"+inputId).val('');
  }

  uploadProof(event,type,buttonId){
    this.serverResponse = "";
    this.disableButton(buttonId);
    let idProof = Object.assign({}, this.proofList[type]);
    idProof['file'] = event.target.files[0];
    idProof['url'] = this.proofList[type]['url']+ event.target.files[0].name;
    if(event.target.files[0].size >= 2097152 ){
      //Big file Size
      this.serverResponse = "File size too big. Max file size allowed is 2 MB";
      this.enableButton(buttonId)
    }else{
      this.ajaxService.uploadFileToSession(idProof)
      .then(
        uploadResponse=>{
          if(uploadResponse && uploadResponse["status"]=="success"){
            this.ajaxService.submitUploadedFiles()
            .then(submitResponse=>{
              this.enableButton(buttonId);
              if(submitResponse["status"] == "success"){
                this.serverResponse = "DOCUMENT_SUBMIT_SUCCESS";
                this.proofSubmitted.emit();
              }else{
                this.serverResponse = submitResponse && submitResponse["message"] ? submitResponse["message"] : "Something went wrong. Please try agian";
              }
            })
          }else if(uploadResponse && uploadResponse["status"]=="error"){
          this.serverResponse = uploadResponse["message"]
          this.enableButton(buttonId)
        }else{
          this.serverResponse = "Something went wrong. Please try again later";
          this.enableButton(buttonId)
        }
        }
      )
    }
  }

  disableButton(buttonId){
    $("#"+buttonId).prop("disabled", true)
    $("#"+buttonId).parent().prop("disabled", true).addClass('btn-progress').addClass('disabled');
  }

  enableButton(buttonId){
    $("#"+buttonId).prop("disabled", false)
    $("#"+buttonId).parent().prop("disabled", false).removeClass('btn-progress').removeClass('disabled');
  }

}
