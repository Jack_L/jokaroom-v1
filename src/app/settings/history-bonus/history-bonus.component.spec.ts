import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryBonusComponent } from './history-bonus.component';

describe('HistoryBonusComponent', () => {
  let component: HistoryBonusComponent;
  let fixture: ComponentFixture<HistoryBonusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryBonusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryBonusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
