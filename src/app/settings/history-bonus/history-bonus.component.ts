import { Component, OnInit, SimpleChange, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-history-bonus',
  templateUrl: './history-bonus.component.html',
  styleUrls: ['./history-bonus.component.scss']
})
export class HistoryBonusComponent implements OnInit {

  @Input() bonusHistoryList;
  @Output() getBonusDetails = new EventEmitter<String>();

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange }): void {
    changes['bonusHistoryList'] && changes['bonusHistoryList'].currentValue ? this.bonusHistoryList = changes['bonusHistoryList'].currentValue : '';
  }

}
