import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { MyProfileGenralComponent } from './my-profile-genral/my-profile-genral.component';
import { MyProfileChangePasswordComponent } from './my-profile-change-password/my-profile-change-password.component';
import { BalanceComponent } from './balance/balance.component';
import { KycComponent } from './kyc/kyc.component';
import { AppTransactions } from './transactions/transactions.component';
import { GameTransactions } from './game-transactions/game-transactions.component';
import { AccountStatement } from './account-statement/account-statement.component';
import { BonusComponent } from './bonus/bonus.component';
import { CurrentBonusComponent } from './current-bonus/current-bonus.component';
import { HistoryBonusComponent } from './history-bonus/history-bonus.component';
import { SharedModule } from '../shared-modules/shared.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule.forRoot()
  ],
  declarations: [
    SettingsComponent,
    MyProfileComponent,
    MyProfileGenralComponent,
    MyProfileChangePasswordComponent,
    BalanceComponent,
    KycComponent,
    AppTransactions,
    GameTransactions,
    AccountStatement,
    BonusComponent,
    CurrentBonusComponent,
    HistoryBonusComponent
  ],
  exports:[SettingsComponent]
})
export class SettingsModule { }
