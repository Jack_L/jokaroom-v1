import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyProfileGenralComponent } from './my-profile-genral.component';

describe('MyProfileGenralComponent', () => {
  let component: MyProfileGenralComponent;
  let fixture: ComponentFixture<MyProfileGenralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyProfileGenralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyProfileGenralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
