import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Utility } from '../utility/utility';
import * as $ from 'jquery';
import { AjaxService } from '../services/ajax.service';
import * as _ from 'underscore';
import { depositMethods } from './config/deposit-method-input-fields';
import { withdrawalMethods } from './config/withdrawal-method-input-fields';
import { CustomValidators } from '../utility/custom-validator';
import { environment } from '../../environments/environment';
import { CashierService } from './services/cashier.service';
import { DepositService } from './services/deposit.service';
import { WithdrawService } from './services/withdraw.service';
import { UserDetailsService } from '../services/user-details.service';
import { FormValidationComponent } from '../shared-modules/form-validation/form-validation.component';
import { EmitterService } from '../services/emitter.service';

@Component({
  selector: 'app-cashier',
  templateUrl: './cashier.component.html',
  styleUrls: ['./cashier.component.scss']
})
export class CashierComponent extends FormValidationComponent implements OnInit {

  @Input() txnType;
  @Output() cashierClosed = new EventEmitter<String>();
  form:FormGroup;
  availableMethods = undefined;
  selectedMethod= undefined;
  selectedMethodInputs = undefined;
  selectedMethodName = undefined;
  selectedMethodCashierData = undefined;
  indexedAvailableMethods;
  fee;
  minAmount;
  maxAmount;
  defaultAmount = true;
  usedAccount = false;
  redirection = false;
  txnStatus = false;
  txnStatusParams;
  pendingWithdrawals;
  userProfileSubscription;
  eligibleBonusSubscription;
  allEligibleBonus;
  eligibleBonus;
  txnId;
  txnRefId;
  userProfile;
  userBalance;
  amounts;
  userCurrency;
  errorMessage;
  cashierDetailsForm = this.fb.group({
    'amount': ['', [this.CustomValidators.required]],
    'merchantId': [ environment.paymentIqMID , [this.CustomValidators.required]],
    'userId': [environment.paymentIqUID, [this.CustomValidators.required]],
    'sessionId': ['1234',this.CustomValidators.required]
  });
  cashierAttributesForm = this.fb.group({
    'successUrl': [''],
    'failureUrl': [''],
    'pendingUrl': [''],
    'labelId': [''],
    'productId': [''],
    'ipAddr': [''],
    'bnsCode': ['']
  })
  constructor(
    private utils:Utility,
    private ajaxService:AjaxService,
    private fb: FormBuilder,
    private CustomValidators:CustomValidators,
    private cashierService:CashierService,
    private depositService:DepositService,
    private userDetailsService:UserDetailsService,
    private emitterService:EmitterService
  ) {
    super(utils);
    this.userProfileSubscription = this.userDetailsService.userProfileUpdated$.subscribe(
      profileUpdateMessage => {
        if(profileUpdateMessage == "UserProfileUpdated" && this.txnType && this.cashierDetailsForm.controls["sessionId"].value != "1234"){
          this.getTransactionTypes();
        }
      }
    );
    this.eligibleBonusSubscription = this.emitterService.eligibleBonus$.subscribe(
      eligibleBonusResponse => {
        if(typeof eligibleBonusResponse == 'string' && eligibleBonusResponse == "loading"){
          this.allEligibleBonus = undefined;
          this.eligibleBonus = undefined;
        }else{
          if(eligibleBonusResponse && eligibleBonusResponse.length > 0){
            this.allEligibleBonus = eligibleBonusResponse;
            this.filterBonus(this.cashierDetailsForm.controls["amount"].value);
          }else{
            this.allEligibleBonus = [];
            this.eligibleBonus = [];
          }
        }
      }
    );
  }

  ngOnInit() {
    this.utils.openModal("cashierModal","");
    //this.cashierDetailsForm.controls["sessionId"].setValue("1234546")
    let promiseQueue = []
    promiseQueue.push(Promise.resolve(this.ajaxService.getCashierAccessToken()));
    if(false){//this.txnType == "withdrawal"
      promiseQueue.push(Promise.resolve(this.ajaxService.getAccountVerificationStatus()));
    }

    Promise.all(promiseQueue)
    .then(
      tokenAndVerificationStatusResponse => {
        let tokenResponse = tokenAndVerificationStatusResponse [0];
        let verificationStatus;
        if(promiseQueue.length >= 2){
          verificationStatus = tokenAndVerificationStatusResponse [1];
        }

        if(false){//this.getVerificationStatus(verificationStatus) != "verified" && this.txnType == "withdrawal"
          this.errorMessage = "Please complete KYC to continue withdrawal";
          this.availableMethods = []
        }else {
          if(tokenResponse && tokenResponse["status"] &&  tokenResponse["status"] == "SUCCESS" && tokenResponse["token"]){
            this.cashierDetailsForm.controls["sessionId"].setValue(tokenResponse["token"])
            this.cashierAttributesForm.controls["successUrl"].setValue(tokenResponse["deposit_succes_callback_url"]);
            this.cashierAttributesForm.controls["failureUrl"].setValue(tokenResponse["deposit_succes_callback_url"]);
            this.cashierAttributesForm.controls["pendingUrl"].setValue(tokenResponse["deposit_succes_callback_url"]);
            this.cashierAttributesForm.controls["labelId"].setValue(tokenResponse["labelId"]);
            this.cashierAttributesForm.controls["productId"].setValue(tokenResponse["productId"]);
            this.cashierAttributesForm.controls["ipAddr"].setValue(tokenResponse["ipAddr"]);
            this.getTransactionTypes();
          }else{
            this.errorMessage = "Authentication failed"
            this.availableMethods = []
          }
        }
      }
    );
  }

  getVerificationStatus(verificationData){
    let kycVerified;
    if(verificationData && verificationData["documents"]){
        let addressVerified  = this.setVerificationStatus(verificationData["documents"].addressStatus);
        let idVerified = this.setVerificationStatus(verificationData["documents"].identityStatus);
        if(verificationData["documents"].addressStatus == "vrfn_verified" && verificationData["documents"].identityStatus == "vrfn_verified"){
          kycVerified =  "verified" ;
        }else if((verificationData["documents"].addressStatus == "vrfn_init" || verificationData["documents"].identityStatus == "vrfn_init") || (verificationData["documents"].addressStatus == "vrfn_failed" || verificationData["documents"].identityStatus == "vrfn_failed")){
          kycVerified =  "vrfn_init" ;
        }else{
          kycVerified =  "not_verified" ;
        }
      }else{
        kycVerified =  "new" ;
      }
      return kycVerified;
  }

  setVerificationStatus(status){
    let response;
    switch(status){
      case 'vrfn_init':
        response = "initiated";
      break;
      case 'vrfn_failed':
        response = "failed";
      break;
      case 'vrfn_verified':
        response = "verified";
      break;
    }
    return response;
  }

  getTransactionTypes(){
    if(this.userDetailsService.getUserProfileDetails()){
      this.userProfile = this.userDetailsService.getUserProfileDetails();
      this.userBalance = this.userDetailsService.getUserTotalBalance();
      this.cashierDetailsForm.controls["userId"].setValue(this.userDetailsService.getUserProfileDetails().playerID);
      this.userCurrency = this.userDetailsService.getUserCurrencyDetails() ? this.userDetailsService.getUserCurrencyDetails().symbol :
      '$';
      if(this.txnType != "transaction"){
        this.changeTxntype(this.txnType);
      }else{
        this.txnId = atob(sessionStorage.getItem("_tid"));
        this.txnRefId = atob(sessionStorage.getItem("_trid"));
        this.selectedMethodName = atob(sessionStorage.getItem("_pmds"));
        this.txnType = atob(sessionStorage.getItem("_txty"));
        this.getTransactionResult();
      }
    }else{
      Promise.resolve(this.ajaxService.getProfileData())
      .then(
        profileData=>{
          this.userDetailsService.setUserCashBalance(profileData["balanceDetails"]["cash"]);
          this.userDetailsService.setUserBalanceDetails(profileData["balanceDetails"]);
          this.userDetailsService.setUserBonusBalance(profileData["balanceDetails"]["bonus"]);
          this.userDetailsService.setUserTotalBalance(profileData["balanceDetails"]["cash"] + profileData["balanceDetails"]["bonus"]);
          this.userDetailsService.setUserProfileDetails(profileData);
        }
      )
    }
  }

  closeModal(modalId){
    this.utils.closeModal(modalId,"",true);
    this.cashierClosed.emit('closeCashier');
  }

  closeAndShowAllTransactions(modalId){
    this.utils.closeModal(modalId,"",true);
    this.cashierClosed.emit('closeCashier/openTransactions');
  }

  changeTxntype(txnType){
    this.txnType = txnType;
    this.redirection = false;
    this.txnStatus = false;

    //this.getPaymentMethod(txnType);
    Promise.resolve(this.cashierService.getPaymentMethod(this.txnType,this.cashierDetailsForm.controls["sessionId"].value,this.cashierDetailsForm.controls["userId"].value))
    .then(
      paymentMethodData => {
        if(paymentMethodData){
          this.assignTHISValues(paymentMethodData);
          this.calculateDefaultAmount(this["availableMethods"].length > 0 ? this["availableMethods"][0].providerType : '');
        }else{
          this.availableMethods = [];
        }
      }
    )
  }
  calculateDefaultAmount(provider){
    var currencyMultiplier = 1;
    let currency = this.userProfile.currency;
    if(currency === "SEK" || currency === "NOK") {
      currencyMultiplier = 10
    }
    let amounts = [];
    let balance = this.userBalance;
    if(this.txnType == 'deposit'){

      if (provider === "FLEXEPIN") {
        amounts = ["20", "50", "100", "250", "500"];
      } else if (balance >= (0 * currencyMultiplier) && balance < (31 * currencyMultiplier)) {
        amounts = [ "20", "30", "50", "100", "200" ];
      } else if (balance >= (30 * currencyMultiplier) && balance < (50 * currencyMultiplier)) {
        amounts = [ "30", "50", "75", "100", "200" ];
      } else if (balance >= (50 * currencyMultiplier) && balance < (100 * currencyMultiplier)) {
        amounts = [ "50", "100", "150", "200", "500" ];
      } else if (balance >= (100 * currencyMultiplier) && balance < (200 * currencyMultiplier)) {
        amounts = [ "100", "150", "200", "300", "500" ];
      } else if (balance >= (200 * currencyMultiplier) ) {
        amounts = [ "100", "250", "400", "500", "1000" ];
      } else {
        amounts = [ "20", "30", "50", "100", "200" ];
      }
      this.setAmount(amounts[0])
    }else{
      if (balance >= 30 && balance < 40) {
        amounts = [
          this.calculateWithdrawalAmount(balance, 5, 0.75),
          this.calculateWithdrawalAmount(balance, 5, 0.9)
        ];
      } else if (balance >= 40 && balance < 80) {
        amounts = [
          this.calculateWithdrawalAmount(balance, 5, 0.5),
          this.calculateWithdrawalAmount(balance, 5, 0.75)
        ];
      } else if (balance >= 80 && balance < 334) {
        amounts = [
          this.calculateWithdrawalAmount(balance, 5, 0.25),
          this.calculateWithdrawalAmount(balance, 5, 0.75)
        ];
      } else if (balance >= 334 && balance < 6750) {
        amounts = [
          this.calculateWithdrawalAmount(balance, 5, 0.3),
          this.calculateWithdrawalAmount(balance, 100, 0.75)
        ];
      } else if (balance >= 6750 && balance < 10000) {
        amounts = [
          this.calculateWithdrawalAmount(balance, 5, 0.3),
          this.calculateWithdrawalAmount(balance, 100, 0.75)
        ];
      }
      if(amounts.length > 0) this.setAmount(amounts[0])
      else this.setAmount('other');
    }
    this.amounts = amounts;
  }

  getButtonDisabled(){
    if(this.form && this.form.valid && this.cashierDetailsForm.valid && this.selectedMethodName){
      return false;
    }else{
      return true;
    }
  }

  calculateWithdrawalAmount(balance, roundedTo, multiplier) {
    var amount = balance*multiplier;

    return Math.floor(amount / roundedTo) * roundedTo;
  }

  setAmount(amount){
    if(amount != 'other'){
      this.cashierDetailsForm.controls["amount"].setValue(amount);
      this.defaultAmount = true;
      this.filterBonus(amount);
    }else{
      this.cashierDetailsForm.controls["amount"].setValue('');
      this.defaultAmount = false;
    }
  }

  makeTransaction(buttonId){
    this.utils.disableButton(buttonId,"",true);
    // if(this.selectedMethodName.toLowerCase() == "instadebit" || this.selectedMethodName.toLowerCase() == "upaycard" || this.selectedMethodName.toLowerCase() == "cashlib" || this.selectedMethodName.toLowerCase() == "interac" || this.selectedMethodName.toLowerCase() == "citadel"){
    //   this.cashierAttributesForm.controls["successUrl"].setValue(window.location.origin+"/en?redirect=transaction");
    //   this.cashierAttributesForm.controls["failureUrl"].setValue(window.location.origin+"/en?redirect=transaction");
    //   this.cashierAttributesForm.controls["pendingUrl"].setValue(window.location.origin+"/en?redirect=transaction");
    // }
    Promise.resolve(this.cashierService.makeTransaction(this.form,this.cashierDetailsForm,this.selectedMethod,this.txnType,this.cashierAttributesForm))
    .then(
      cashierResp =>{
        if(cashierResp && cashierResp["status"] == "success" && cashierResp["response"]){
          let txnStatus = cashierResp["response"];
          this.txnId = txnStatus["txRefId"].substr(txnStatus["txRefId"].indexOf('A')+1);
          this.txnRefId = txnStatus["txRefId"];
          if(txnStatus["success"] && txnStatus["txState"] && txnStatus["txState"] == "WAITING_INPUT"){
            let contentAndForm = this.depositService.getIframe(txnStatus["redirectOutput"],this.selectedMethodName);
            let $content = contentAndForm.content;
            let $form = contentAndForm.form;
            this.txnStatus = false;
            this.redirection = true;
            sessionStorage.setItem("_tid",btoa(this.txnId));
            sessionStorage.setItem("_trid",btoa(this.txnRefId));
            sessionStorage.setItem("_pmds",btoa(this.selectedMethodName));
            sessionStorage.setItem("_txty",btoa(this.txnType));
            setTimeout(function(){
              $("#redirectionDiv").append($content);
              var $iframe = $('#loaderIframe');
              $iframe.ready(function() {
                  $iframe.contents().find("body").length >0 ? $iframe.contents().find("body").append($form) : $iframe.append($form);
                  if(!txnStatus["redirectOutput"]["html"] && ($("#loaderIframe").contents().find("#proxy_redirect").length || $("#loaderIframe").find("#proxy_redirect").length))
                    $("#loaderIframe").contents().find("#proxy_redirect").length > 0 ? $("#loaderIframe").contents().find("#proxy_redirect").submit() : $("#loaderIframe").find("#proxy_redirect").submit();
              });

            },1)
          }else {
            this.getTransactionResult()
          }
        }
      },
      SystemError=>{
        this.utils.enableButton(buttonId,"");
      }
    )
  }

  setFeeAndLimit(method){
    let feeResponse = this.cashierService.setFeeAndLimit(method);
    this.fee = feeResponse["fee"]
    this.minAmount = feeResponse["minAmount"]
    this.maxAmount = feeResponse["maxAmount"]
  }

  dynamicFormButtonClicked(message){
    this.getFormFieldsForMethod(this.selectedMethodName,this.selectedMethodCashierData,message,this.txnType)
  }

  getFormFieldsForMethod(selectedMethodName,selectedMethodCashierData,message,txnType){
    let formFieldsResponse = this.cashierService.getFormFieldsForMethod(selectedMethodName,selectedMethodCashierData,message,txnType)
    this.assignTHISValues(formFieldsResponse);
  }

  assignTHISValues(object){
    var self = this;
    _.each(object,function(value,key){
      self[key] = value
    })
    if(this.form && this.form.controls["cardHolder"] && this.userDetailsService.getUserProfileDetails()){
      let cardHolderName = this.userDetailsService.getUserProfileDetails().firstName +" "+this.userDetailsService.getUserProfileDetails().lastName;
      this.form.controls["cardHolder"].setValue(cardHolderName);
      this.form.controls["cardHolder"].markAsTouched({onlySelf:true});
      this.form.controls["cardHolder"].markAsDirty({onlySelf:true});
    }

    let limit;
    if(this.selectedMethodCashierData && this.selectedMethodCashierData.limit && this.selectedMethodCashierData.limit.min && this.selectedMethodCashierData.limit.max){
      limit = this.selectedMethodCashierData.limit
    }else{
      limit ={
        min : null,
        max : null
      }
    }

    let userCashBalance = this.userDetailsService.getUserCashBalance();

    if(this.txnType == 'withdrawal' && userCashBalance && limit.max > userCashBalance){
      limit.max = userCashBalance;
    }else if(this.txnType == 'withdrawal' && userCashBalance && limit.min > userCashBalance){
      limit.min = userCashBalance;
      limit.max = userCashBalance;
    }else if(this.txnType == 'withdrawal' && userCashBalance && !limit.min && !limit.max ){
      limit.min = 0;
      limit.max = userCashBalance;
    }

    this.cashierDetailsForm.controls["amount"].setValidators(this.CustomValidators.minValueNumber(Number(limit.min),Number(limit.max),"Amount"));


    if(this.form && this.form.controls["countryCode"] && this.userDetailsService.getUserProfileDetails()){
      this.form.controls["countryCode"].setValue(this.userDetailsService.getUserProfileDetails().country)
    }

  }

  reverseWithdrawal(txnId,buttonId){
    this.utils.disableButton(buttonId,"",true);
    Promise.resolve(this.ajaxService.deletePendingTransaction({"sessionId":this.cashierDetailsForm.controls["sessionId"].value},txnId,this.cashierDetailsForm.controls["userId"].value))
    .then(
      deleteTransactionResponse=>{

        if(deleteTransactionResponse && deleteTransactionResponse["success"]){
          Promise.resolve(this.cashierService.getPendingTransactions(this.cashierDetailsForm.controls["sessionId"].value,this.cashierDetailsForm.controls["userId"].value))
          .then(
            pendingtransaction =>{
              this.pendingWithdrawals = this.cashierService.setPendingTransaction(this.txnType,pendingtransaction);
              this.utils.enableButton(buttonId,"");
            }
          )
        }else{
          this.utils.enableButton(buttonId,"");
        }
      }
    )
  }

  getTransactionResult(){
    this.txnStatus = true;
    this.redirection = false;
    sessionStorage.removeItem("_trid");
    sessionStorage.removeItem("_trid");
    sessionStorage.removeItem("_pmds");
    sessionStorage.removeItem("_txty");
    Promise.resolve(this.ajaxService.getUserPendingTransaction({"sessionId":this.cashierDetailsForm.controls["sessionId"].value},this.cashierDetailsForm.controls["userId"].value+"/"+this.txnId+"/status")).then(
      transactionDetails => {
        let txnStatusParams = {}
        if(transactionDetails["success"] && transactionDetails["txState"] && (transactionDetails["txState"] == "SUCCESSFUL" || transactionDetails["txState"] == "WAITING_APPROVAL")){
          _.each(transactionDetails["messages"],function(txndata){
            txnStatusParams[(txndata["keys"][(txndata["keys"].length)-1]).replace("Deposit","").replace("Withdrawal","")] = txndata["value"];
          })
          this.emitterService.broadcastCashierTransactionSuccess("TransactionSuccesss");
        }else if(transactionDetails["success"] == false && transactionDetails["txState"] && transactionDetails["txState"] == "FAILED"){
          txnStatusParams["message"] = transactionDetails["errors"][0].msg;
        }else{
          txnStatusParams["message"] = "Something went wrong. Please try agin.";
        }
        txnStatusParams["txnSuccessOrFail"] = transactionDetails["txState"] ? transactionDetails["txState"] : "FAILED";
        txnStatusParams["txnTxRefId"] = this.txnRefId;
        txnStatusParams["txnDate"] = new Date();
        this.txnStatusParams = txnStatusParams;

      }
    )
  }

  filterBonus(amount){
    let txnAmount = Number(amount);
    var self = this;
    self.eligibleBonus = self.allEligibleBonus;
    // if(self.allEligibleBonus && self.userProfile){
    //   _.each(self.allEligibleBonus,function(bonus){
    //     if(bonus.currencyTier[self.userProfile.currency]){
    //       _.every(bonus.currencyTier[self.userProfile.currency],function(tier){
    //         if(tier.minDepositValue/100 <= txnAmount && tier.maxDepositValue/100 >= txnAmount){
    //           self.eligibleBonus.push(bonus);
    //           return false;
    //         }else{
    //           return true;
    //         }
    //       })
    //     }
    //   })
    // }
  }

  ngOnDestroy(){
    $("body>#cashierModal").remove();
    this.userProfileSubscription.unsubscribe();
  }

}
