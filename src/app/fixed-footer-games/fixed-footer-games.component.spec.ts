import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedFooterGamesComponent } from './fixed-footer-games.component';

describe('FixedFooterGamesComponent', () => {
  let component: FixedFooterGamesComponent;
  let fixture: ComponentFixture<FixedFooterGamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedFooterGamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedFooterGamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
