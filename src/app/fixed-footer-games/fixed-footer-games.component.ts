import { Component, OnInit, HostListener, NgZone, Renderer2, ViewChild,ElementRef, Input, SimpleChange } from '@angular/core';
import { GameService } from '../services/game.service';
import { EmitterService } from '../services/emitter.service';
import { environment } from '../../environments/environment';
import { Utility } from '../utility/utility';
import * as $ from 'jquery';
import * as _ from 'underscore';

@Component({
  selector: 'app-fixed-footer-games',
  templateUrl: './fixed-footer-games.component.html',
  styleUrls: ['./fixed-footer-games.component.scss']
})
export class FixedFooterGamesComponent implements OnInit {
  @Input() isGameLaunch;
  featuredGames;
  hotGames;
  allFavGames;
  favUpdateSubs;
  indexedGameData;
  lastPlayedData = [];
  lastPlayedSubs;
  hostName;
  websiteCode;
  loginSubs;
  isUserLoggedIn;
  logoutSubs;
  @ViewChild('timer')
  public timerElem: ElementRef;
  constructor(
    private gameService:GameService,
    private emitterService:EmitterService,
    private zone: NgZone,
    private renderer: Renderer2,
    private utils:Utility
  ) {
    this.loginSubs = this.emitterService.loginComplete$.subscribe(
      loginStatus => {
        if (loginStatus == "LoginCompleted") {
          this.isUserLoggedIn = true;
        }
      }
    )

    this.logoutSubs = this.emitterService.logoutComplete$.subscribe(
      logoutStatus => {
        if (logoutStatus == "LogoutCompleted") {
          this.isUserLoggedIn = false;
        }
      }
    )

    this.zone.runOutsideAngular(() => {
      setInterval(() => {
        this.setDateInFooter();
      },1000);
    });

    this.favUpdateSubs = this.emitterService.favGameUpdate$.subscribe(
      favGameStatus => {
        if(favGameStatus == "UPDATED"){
          this.setFavGame();
        }
      }
    )

    this.lastPlayedSubs = this.emitterService.lastPlayedUpdated$.subscribe(
      lastPlayedStatus => {
        if(lastPlayedStatus == "LastPlayUpdated"){
          this.setLastPlayed();
        }
      }
    )

  }

  ngOnInit() {
    this.setDateInFooter();
    this.hostName = environment.siteUrl;
    this.websiteCode = environment.websiteCode;
    this.isUserLoggedIn = this.utils.isUserLoggedIn();
    Promise.resolve(this.gameService.getGameAllData())
    .then(
      gameAllData =>{
        this.featuredGames = _.filter(gameAllData["rawGameData"],function(game){
              return game.isFeatured;
        });
        this.hotGames = _.filter(gameAllData["rawGameData"],function(game){
              return game.isHot;
        });

        this.indexedGameData = gameAllData["indexedGameData"];
        this.setLastPlayed();
        this.allFavGames = _.toArray(gameAllData["allFavGames"]);
      }
    );
  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange
  }): void {
    changes['isGameLaunch'] ? this.isGameLaunch = changes['isGameLaunch'].currentValue : '';
  }

  setDateInFooter(){
    let date =  new Date();
    let mins = date.getMinutes()
    this.renderer.setProperty(this.timerElem.nativeElement, 'textContent', date.getHours()+":"+(mins > 9 ? mins : '0'+mins));
  }

  setLastPlayed(){
    var self = this;
    if(this.indexedGameData){
      let LastPlayedGames = this.gameService.getAllLastPlayed();
      self.lastPlayedData = [];
      _.each(LastPlayedGames,function(game){
        if(self.indexedGameData[game]){
          self.lastPlayedData.push(self.indexedGameData[game])
        }
      })
    }else{
      Promise.resolve(this.gameService.getGameAllData())
      .then(
        gameAllData =>{
          this.indexedGameData = gameAllData["indexedGameData"];
          this.setLastPlayed();
        }
      )
    }
  }

  setFavGame(){
    this.allFavGames = _.toArray(this.gameService.getAvailableFavGames());
  }

  @HostListener('wheel',['$event'])
  Wheel(e){
      var delta = Math.max(-1, Math.min(1, (e.deltaY || -e.detail)));
      var self = this;
      //featured
      if(self.featuredGames && $('#f-games-container').scrollLeft() - delta * 40 < (self.featuredGames.length * 115 - $(window).width() + 50))
      	$('#f-games-container').scrollLeft($('#f-games-container').scrollLeft() - (delta * 40)); // Multiplied by 40
      //hot
      if(self.hotGames && $('#hot-games-container').scrollLeft() - delta * 40 < (self.hotGames.length * 115 - $(window).width() + 50))
        $('#hot-games-container').scrollLeft($('#hot-games-container').scrollLeft() - (delta * 40)); // Multiplied by 40
      //Last played
      if(self.lastPlayedData && $('#last-games-container').scrollLeft() - delta * 40 < (self.lastPlayedData.length * 115 - $(window).width()  + 20))
      	$('#last-games-container').scrollLeft($('#last-games-container').scrollLeft() - (delta * 40)); // Multiplied by 40
      //favourites
      if(self.allFavGames && $('#fav-games-container').scrollLeft() - delta * 40 < (self.allFavGames.length * 115 - $(window).width() + 20))
        $('#fav-games-container').scrollLeft($('#fav-games-container').scrollLeft() - (delta * 40)); // Multiplied by 40
      e.preventDefault();
    }

  ngAfterViewInit() {

      $('ul.f-tabs li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.f-tabs li').removeClass('current');
    $('.f-tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  })




    $('#fixed-footer-tabs a').on('click', function() {
      if (!$('.fixed-footer').hasClass('opened')) {
        $('.fixed-footer').toggleClass('opened');
        $("#closeButton").removeClass('hide').addClass('show');
      }
    });
  }

  closeFixedFooter($event) {
    this.utils.closeFixedFooter($event)
  }

  openFixedFooter($event) {
    var path = $event.path || ($event.composedPath && $event.composedPath());
    if(!path){
      path = []
      let target = $(event.target)

      while (target.parent() && target.parent().length > 0) {
        path.push(target)
        target = $(target.parent());
      }

      path.push(document, window)
    }
    let pos = path.length - 10;
    if(!$('.fixed-footer').hasClass('opened') && !$($event.srcElement).data("tab") && !(path[pos] && $(path[pos]).hasClass("quick-d-f"))){
      $("#featuredTabLink").addClass('current');
      $("#featured").addClass('current');
    }

    if($($event.srcElement).data("tab") != "timer"  && !(path[pos] && $(path[pos]).hasClass("quick-d-f"))){
      $('.fixed-footer').addClass('opened');
      $("#closeButton").removeClass('hide').addClass('show');
    }
    $event.stopPropagation();
  }

  openGame($event,gameId){
    this.utils.closeFixedFooter($event)
    this.gameService.goToGame(gameId,'');
  }

  ngOnDestroy(){
      this.favUpdateSubs.unsubscribe();
      this.loginSubs.unsubscribe();
      this.logoutSubs.unsubscribe();
  }

}
