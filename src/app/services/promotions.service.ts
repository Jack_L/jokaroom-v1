import { Injectable } from '@angular/core';
import { AjaxService } from '../services/ajax.service';
import { Utility } from '../utility/utility'
import { EmitterService } from './emitter.service';
import * as _ from 'underscore';

@Injectable()
export class PromotionsService {
  private promotionsData;
  private idIndexedPromotions;
  private forcePromo = false;
  cashierTransactionSubs;
  constructor(
    private ajaxService:AjaxService,
    private utils:Utility,
    private emitterService:EmitterService
  ) {
    this.cashierTransactionSubs = this.emitterService.cashierTransactionSuccess$.subscribe(
      transactionSuccessData => {
        if (transactionSuccessData == "TransactionSuccesss") {
          var self = this;
          setTimeout(function(){
            self.forcePromo = true;
            self.emitterService.broadcastGetBannerAndPromo("TimeOutOver");
          },5000)
        }
      }
    )
  }

  setPromotionsData(promotionsData){
    this.promotionsData = promotionsData
  }

  setIdIndexedPromotionsData(idIndexedPromotions){
    this.idIndexedPromotions = idIndexedPromotions
  }

  getPromotionsData(isForce){
    if(!isForce && this.promotionsData && !this.forcePromo){
      return this.promotionsData
    }else{

      return Promise.resolve(this.ajaxService.getPromos(this.getDataToSend()))
      .then(
        promoData=>{
          this.setPromotionsData(promoData);
          this.setIdIndexedPromotionsData(_.indexBy(promoData,'url'));
          this.forcePromo = false;
          return this.promotionsData;
        }
      );
    }
  }

  getIndexedPromotionData(isForce){
    if(this.idIndexedPromotions && !isForce && !this.forcePromo){
      return this.idIndexedPromotions
    }else{
      return Promise.resolve(this.ajaxService.getPromos(this.getDataToSend()))
      .then(
        promoData=>{
          this.setPromotionsData(promoData);
          this.setIdIndexedPromotionsData(_.indexBy(promoData,'url'));
          this.forcePromo = false;
          return this.idIndexedPromotions;
        }
      );
    }
  }

  getDataToSend(){
    let promoData ={}
    let affId = this.utils.getAffIdCookie("affIdCookie");
    if(affId){
      promoData["affiliateId"] = affId;
    }
    return promoData;
  }

}
