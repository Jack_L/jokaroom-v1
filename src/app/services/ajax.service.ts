import { Injectable } from '@angular/core';
import { URLSearchParams } from "@angular/http";
import {Http,Headers} from '@angular/http';
import { environment } from '../../environments/environment';
import { EmitterService } from './emitter.service';

import { Router } from '@angular/router';
import * as _ from 'underscore';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

@Injectable()
export class AjaxService {

  header;

  constructor(
    private http:Http,
    private emitterService:EmitterService
  ) {
    this.header = new Headers({ "content-type": "application/x-www-form-urlencoded", "accept": "application/json, text/plain, */*"});
  }

  psPost(url,params): Promise<String>{
      let self =this;
    	let loginMessage:string = "";
      if(url.includes('upload')){
        return this.http.post(environment.apiUrl+url,params,{withCredentials: true})
        .toPromise().then(
            data => {
              let response = JSON.parse(data["_body"]);
              if(response && response.is_IpBlocked && response.is_IpBlocked == "true"){
                throw new Error("countryBlocked:"+response.countryCode);
              }else if(response && response["session_expired_sso"] ){
                throw new Error("session_expired_sso:"+response.session_expired_sso);
              }else{
                return response;
              }
            }
          ).catch(e =>{
            this.handleHTTPError(e);
        });
     }else{
      return this.http.post(environment.apiUrl+url,params,{withCredentials: true,headers:this.header})
      .toPromise().then(
          data => {
            let response = JSON.parse(data["_body"]);
            if(response && response.is_IpBlocked && response.is_IpBlocked == "true"){
              throw new Error("countryBlocked:"+response.countryCode);
            }else if(response && response["session_expired_sso"] ){
              throw new Error("session_expired_sso:"+response.session_expired_sso);
            }else{
              return response;
            }
          }
        ).catch(e =>{
          this.handleHTTPError(e);
        });
     }
  }

  psGet(searchParams,url): Promise<String>{
      return new Promise((resolve,reject)=>{
        let self =this;
        this.http.get(environment.apiUrl+url,{withCredentials: true,search:searchParams})
        .toPromise().then(
            data => {
              let response;
              if(url.includes("gamelauncher") || url.includes("freegames")){
                  response = data["_body"];
              }else{
                response = JSON.parse(data["_body"]);
              }

              if(response && response["is_IpBlocked"] && response["is_IpBlocked"] == "true"){
                throw new Error("countryBlocked:"+response.countryCode);
              }else if(response && response["session_expired_sso"] ){
                throw new Error("session_expired_sso:"+response.session_expired_sso);
              }else{
                resolve (response);
              }
            }
          ).catch(e =>{
            this.handleHTTPError(e);
          });
      })

  }

  paymentIQPost(url,params){
    return this.http.post(environment.paymentIqUrl+url,params)
    .toPromise().then(
      data => {
        let response = JSON.parse(data["_body"]);
        if(response && response.is_IpBlocked && response.is_IpBlocked == "true"){
          throw new Error("countryBlocked:"+response.countryCode);
        }else if(response && response["session_expired_sso"] ){
          throw new Error("session_expired_sso:"+response.session_expired_sso);
        }else{
          return response;
        }
      }
    ).catch(e =>{
      this.handleHTTPError(e);
    });
  }

  paymentIQDelete(url,params,urlParams,userId){
    return this.http.delete(environment.paymentIqUrl+url+environment.paymentIqMID+"/"+userId+urlParams,{params})
    .toPromise().then(
      data => {
        let response = JSON.parse(data["_body"]);
        if(response && response.is_IpBlocked && response.is_IpBlocked == "true"){
          throw new Error("countryBlocked:"+response.countryCode);
        }else if(response && response["session_expired_sso"] ){
          throw new Error("session_expired_sso:"+response.session_expired_sso);
        }else{
          return response;
        }
      }
    ).catch(e =>{
      this.handleHTTPError(e);
    });
  }

  paymentIQGet(searchParams,url,userId){
    return this.http.get(environment.paymentIqUrl+url+environment.paymentIqMID+"/"+userId,{search:searchParams})
    .toPromise().then(
      data => {
        let response = JSON.parse(data["_body"]);
        if(response && response.is_IpBlocked && response.is_IpBlocked == "true"){
          throw new Error("countryBlocked:"+response.countryCode);
        }else if(response && response["session_expired_sso"] ){
          throw new Error("session_expired_sso:"+response.session_expired_sso);
        }else{
          return response;
        }
      }
    ).catch(e =>{
      this.handleHTTPError(e);
    });
  }

  handleHTTPError(e){
    if(e.message && e.message.includes("countryBlocked")){
      this.emitterService.broadcastCountryBlocked(e.message.split(":")[1])
    }else if(e.message && e.message.includes("session_expired_sso") ){
      this.emitterService.broadcastSSOError(e.message.split(":")[1]);
    }else{
      if (e.status === 401) {
      }else if (e.status === 404) {
      }else if(e.status === 504){
      }else{

      }
    }
  }

  getUrlParams(fields):URLSearchParams{
    let urlParams = new URLSearchParams();
    _.each(fields, function(value,key) {
        urlParams.append(key, value);
    });
    return urlParams;
  }

  getSearchParams(searchData){
    let searchParams = new URLSearchParams();
    _.each(searchData, function(value,key) {
        searchParams.set(key, value);
    });
    return searchParams;
  }

  getLoginStatus(): Promise<String>{
    return this.psGet("",'/ajax/login/status');
  }

  doLogin(credentials): Promise<String>{
    return this.psPost('/ajax/login',this.getUrlParams(credentials));
  }

  doLogout(): Promise<String>{
    return this.psPost('/ajax/login/logout',{});
  }

  forgotPassword(emailDetails): Promise<String>{
    return this.psPost('/ajax/resetPassword',this.getUrlParams(emailDetails));
  }

  resetPassword(pwdDetails): Promise<String>{
    return this.psPost('/ajax/resetPassword/doResetPassword',this.getUrlParams(pwdDetails));
  }

  getProfileBalanceCurrency(): Promise<String>{
    return this.psGet("",'/ajax/profile/getProfileBalanceCurrency');
  }

  getCountries(): Promise<String>{
    return this.psGet("",'/ajax/profile/getcountryDetails');
  }

  getEnabledCuurencies(): Promise<String>{
    return this.psGet("",'/ajax/profile/getEnabledCurrency');
  }

  validateUniqueness(fieldToValidate): Promise<String>{
    let url;
    _.each(fieldToValidate, function(value,key) {
        url = key == "txtNickname" ? '/ajax/registration/isUniqueNickname' : '/ajax/registration/isUniqueEmail';
    });
    return this.psPost(url,this.getUrlParams(fieldToValidate));
  }

  doRegistration(registrationDetails): Promise<String>{
    return this.psPost('/ajax/registration',this.getUrlParams(registrationDetails));
  }

  doProfileUpdate(UpdateProfileDetails): Promise<String>{
    return this.psPost('/ajax/profile/update',this.getUrlParams(UpdateProfileDetails));
  }

  getProfileData(): Promise<String>{
    return this.psGet("",'/ajax/profile/getData');
  }

  getBalance(): Promise<String>{
    return this.psGet("",'/ajax/profile/getBalance');
  }

  changePassword(changePasswordData): Promise<String> {
    return this.psPost('/ajax/profile/changePassword',this.getUrlParams(changePasswordData));
  }

  getAccountVerificationStatus(): Promise<String>{
    return this.psGet("",'/ajax/profile/getAccountVerificationStatus');
  }

  uploadFileToSession(idProof):Promise<String>{
    let formData:FormData = new FormData();
		formData.append('file', idProof.file);
    return this.psPost(idProof.url,formData);
  }

  submitUploadedFiles(){
    return this.psGet("","/ajax/account/Documents/send");
  }

  getGames(){
    return this.psGet("","/ajax/game/getGames");
  }

  getFavouriteGames(){
    return this.psPost("/ajax/profile/getFavouriteGames","");
  }

  isBlockedCountry():Promise<any>{
    return this.psGet("",'/ajax/country/countryblock');
  }

  lauchGame(gamePlayData):Promise<any>{
   return this.psGet("","/launcher/"+gamePlayData.gameType+"/index/"+gamePlayData.gameId);
 }

  updateSubscriptionPreferenes(subscriptionPrefData): Promise<String>{
    return this.psPost('/ajax/profile/updateSubscriptionPreferenes',this.getUrlParams(subscriptionPrefData));
  }

  getSubscriptionPreferenes(): Promise<String>{
    return this.psPost('/ajax/profile/getSubscriptionPreferenes',"subscriptionPref");
  }

  getUserTransactionsHistory(request):any{
    let txnReq = new URLSearchParams();
    txnReq.append("criteria",JSON.stringify(request));
    return this.psPost('/ajax/Report/getTransactionHistory',txnReq);
  }

  getUserBetHistory(request):any{
    let betReq = new URLSearchParams();
    betReq.append("criteria",JSON.stringify(request));
    return this.psPost('/ajax/Report/getTransactionHistory',betReq);
  }

  getEligibleBonuses(eligibleBonusData): Promise<String>{
    return this.psGet(this.getSearchParams(eligibleBonusData),'/ajax/bonus/getEligibleBonuses');
  }

  getGamePlayUrl(gameType,gameData): Promise<String>{
    return this.psGet(this.getSearchParams(gameData),'/ajax/launcher/'+gameType);
  }

  toggleFavouriteGames(gameFavData){
    return this.psPost('/ajax/profile/toggleFavoriteGame',this.getUrlParams(gameFavData));
  }

  getBanners(bannerData):Promise<any>{
    return this.psGet(this.getSearchParams(bannerData),'/ajax/banner/getBanners');
  }

  getStaticPage(data):Promise<any>{
    return this.psGet(this.getSearchParams(data),'/ajax/staticPage/getPage');
  }

  getPromos(data){
    return this.psGet(this.getSearchParams(data),'/ajax/promotion/getPromotions');
  }

  getActiveBonusForUser(){
    return this.psGet("",'/ajax/bonus/activeBonusDetails');
  }

  getHistoryBonusForUser(){
    return this.psGet("",'/ajax/bonus/getBonusHistory');
  }

  getUserPaymentAccount(cashierMethodData,userId){
    return this.paymentIQGet(this.getSearchParams(cashierMethodData),"/api/user/account/",userId);
  }

  getUserAvailablePaymentMethod(cashierMethodData,userId){
    return this.paymentIQGet(this.getSearchParams(cashierMethodData),"/api/user/payment/method/",userId);
  }

  getUserPendingTransaction(pendingMethodData,userId){
    return this.paymentIQGet(this.getSearchParams(pendingMethodData),"/api/user/transaction/",userId);
  }

  deletePendingTransaction(deleteData,urlParams,userId){
    return this.paymentIQDelete("/api/user/transaction/",this.getSearchParams(deleteData),"/"+urlParams,userId);
  }

  getCashierEntireData(cashierMethodData,userId){
    return this.paymentIQGet(this.getSearchParams(cashierMethodData),"/api/cashier/",userId);
  }

  doDeposit(depositData){
    return this.paymentIQPost('/api/creditcard/deposit/process',depositData);
  }

  doTransaction(txnData,txnType,selectedMethodName){
    return this.paymentIQPost('/api/'+selectedMethodName.toLowerCase()+'/'+txnType+'/process',txnData);
  }

  getCashierAccessToken():Promise<String>{
    return this.psGet(this.getSearchParams({"token":"cashier"}),'/ajax/token/getToken');
  }

  dropBonus(bonusData):Promise<String>{
    return this.psGet(this.getSearchParams(bonusData),'/ajax/bonus/drop');
  }

  getFaqQuestionsAndCategory():Promise<any>{
    return this.psGet("",'/ajax/faq/getQuestions');
  }

  getWinners():Promise<any>{
    return this.psGet("",'/ajax/jackpot/getWinners');
  }

  getWinnersFromNotification(notificationData):Promise<any>{
    let txnReq = new URLSearchParams();
    txnReq.append("response",JSON.stringify(notificationData));
    return this.psGet(txnReq,'/ajax/jackpot/getWinnersFromNotification');
  }

}
