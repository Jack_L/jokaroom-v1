import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { EmitterService } from './emitter.service';
import { AjaxService } from './ajax.service';
@Injectable()
export class UserDetailsService {

  userProfileDetails;
  userCashBalance;
  userBonusBalance;
  userTotalBalance;
  userBalanceDetails;
  userCurrencyDetails;
  userKycDetails;
  userBrowserCountry;
  logoutSubs;

  private userProfileUpdated = new Subject<string>();
  userProfileUpdated$ = this.userProfileUpdated.asObservable();

  private userBalanceUpdated = new Subject<string>();
  userBalanceUpdated$ = this.userBalanceUpdated.asObservable();

  private userKycUpdated = new Subject<string>();
  userKycUpdated$ = this.userKycUpdated.asObservable();

  private userCurrencyUpdated = new Subject<string>();
  userCurrencyUpdated$ = this.userCurrencyUpdated.asObservable();

  constructor(
    private emitterService:EmitterService,
    private ajaxService:AjaxService
  ) {
    this.logoutSubs = this.emitterService.logoutComplete$.subscribe(
      logoutStatus => {
        if (logoutStatus == "LogoutCompleted") {
          this.resetAllDetails();
        }
      }
    )
  }

  setUserProfileDetails(userProfile){
    this.userProfileDetails = userProfile;
    this.userProfileUpdated.next("UserProfileUpdated");
  }

  getUserBrowserCountry(){
    return this.userBrowserCountry;
  }

  setUserBrowserCountry(userBrowserCountry){
    this.userBrowserCountry = userBrowserCountry;
  }

  getUserProfileDetails(){
    return this.userProfileDetails;
  }

  setUserBonusBalance(userBonusBalance){
    this.userBonusBalance = userBonusBalance;
  }

  getUserBonusBalance(){
    return this.userBonusBalance;
  }

  setUserCashBalance(userCashBalance){
    this.userCashBalance = userCashBalance;
  }

  getUserCashBalance(){
    return this.userCashBalance;
  }

  setUserTotalBalance(userTotalBalance){
    this.userTotalBalance = userTotalBalance;
  }

  getUserTotalBalance(){
    return this.userTotalBalance;
  }

  setUserBalanceDetails(userBalanceDetails){
    this.userBalanceDetails = userBalanceDetails;
    this.userBalanceUpdated.next("UserBalanceUpdated");
  }

  getUserBalanceDetails(){
    return this.userBalanceDetails;
  }

  setUserCurrencyDetails(userCurrencyDetails){
    this.userCurrencyDetails = userCurrencyDetails;
    this.userCurrencyUpdated.next("UserCurrencyUpdated");
  }

  getUserCurrencyDetails(){
    return this.userCurrencyDetails;
  }

  setUserKycDetails(userKycDetails){
    this.userKycDetails = userKycDetails;
    this.userKycUpdated.next("UserKycUpdated");
  }

  getUserKycDetails(){
    return this.userKycDetails;
  }

  forceGetUserProfileDetails(){
    Promise.resolve(this.ajaxService.getBalance())
    .then(balanceDetails => {
      if (balanceDetails) {
        this.setUserCashBalance(balanceDetails["cash"]);
        this.setUserBalanceDetails(balanceDetails);
        this.setUserBonusBalance(balanceDetails["bonus"]);
        this.setUserTotalBalance(balanceDetails["cash"] + balanceDetails["bonus"]);
      }
    });
  }

  resetAllDetails(){
    this.userProfileDetails = undefined;
    this.userCashBalance = undefined;
    this.userBonusBalance = undefined;
    this.userTotalBalance = undefined;
    this.userBalanceDetails = undefined;
    this.userCurrencyDetails = undefined;
    this.userKycDetails = undefined;
    this.userBrowserCountry = undefined;
  }

}
