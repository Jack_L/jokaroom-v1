import { Injectable } from '@angular/core';
import { AjaxService } from './ajax.service';
import { EmitterService } from './emitter.service';
import * as _ from 'underscore';

@Injectable()
export class BannerService {
  banners = {};
  getBannersMap = [];
  loginSubs;
  logoutSubs;
  zoneIdForceMap = {};
  cashierTransactionSubs;
  constructor(
    private ajaxService:AjaxService,
    private emitterService:EmitterService
  ) {
    this.loginSubs = this.emitterService.loginComplete$.subscribe(
      loginStatus => {
        if (loginStatus == "LoginCompleted") {
          this.forceAllZoneBanner();
        }
      }
    )

    this.logoutSubs = this.emitterService.logoutComplete$.subscribe(
      logoutStatus => {
        if (logoutStatus == "LogoutCompleted") {
          this.forceAllZoneBanner();
        }
      }
    )

    this.cashierTransactionSubs = this.emitterService.cashierTransactionSuccess$.subscribe(
      transactionSuccessData => {
        if (transactionSuccessData == "TransactionSuccesss") {
          var self = this;
          setTimeout(function(){
            self.forceAllZoneBanner();
            self.emitterService.broadcastGetBannerAndPromo("TimeOutOver");
          },5000)
        }
      }
    )
  }

  getBanners(bannerReqData, isForce):Promise<any>{
    if(this.banners[bannerReqData.zoneId] && !isForce && this.zoneIdForceMap[bannerReqData.zoneId] != undefined && !this.zoneIdForceMap[bannerReqData.zoneId]) return Promise.resolve(this.banners[bannerReqData.zoneId]);
    return new Promise((resolve,reject)=>{
      if(this.getBannersMap[bannerReqData.zoneId] && this.getBannersMap[bannerReqData.zoneId].length > 0){
        this.getBannersMap[bannerReqData.zoneId].push(resolve)
      }else{
        if(!this.getBannersMap[bannerReqData.zoneId]){
          this.getBannersMap[bannerReqData.zoneId] = []
        }
        this.getBannersMap[bannerReqData.zoneId].push(resolve);
        Promise.resolve(this.ajaxService.getBanners(bannerReqData))
        .then(
          banners => {
            this.banners[bannerReqData.zoneId] = banners;
            for(let callback in  this.getBannersMap[bannerReqData.zoneId]){
              var resp_obj = JSON.parse(JSON.stringify(this.banners[bannerReqData.zoneId]));
              this.getBannersMap[bannerReqData.zoneId][callback](resp_obj);
            }
            this.zoneIdForceMap[bannerReqData.zoneId] = false
            this.getBannersMap[bannerReqData.zoneId] = [];
            //return this.banners;
          }
        )
      }


      // if(this.getBannersMap.length > 0){
      //   this.getBannersMap.push(resolve)
      // }else{
      //   this.getBannersMap.push(resolve)
      //
      // }
    });
  }

  forceAllZoneBanner(){
    var self = this;
    _.each(this.zoneIdForceMap,function(value,zoneId){
      self.zoneIdForceMap[zoneId] = true;
    })
  }

}
