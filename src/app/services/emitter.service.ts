import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class EmitterService {

  constructor() { }

  private loginCompleteSource = new Subject<string>();

  loginComplete$ = this.loginCompleteSource.asObservable();

  broadcastloginComplete(loginStatus: string) {
    this.loginCompleteSource.next(loginStatus);
  }

  private logoutCompleteSource = new Subject<string>();

  logoutComplete$ = this.logoutCompleteSource.asObservable();

  broadcastlogoutComplete(logoutStatus: string) {
    this.logoutCompleteSource.next(logoutStatus);
  }

  private doLogoutCompleteSource = new Subject<string>();

  doLogoutComplete$ = this.doLogoutCompleteSource.asObservable();

  broadcastDoLogoutComplete(logoutStatus: string) {
    this.doLogoutCompleteSource.next(logoutStatus);
  }

  private countryBlockedSource = new Subject<string>();

  countryBlocked$ = this.countryBlockedSource.asObservable();

  broadcastCountryBlocked(blockedCOuntry: string) {
    this.countryBlockedSource.next(blockedCOuntry);
  }

  private favGameUpdated = new Subject<string>();

  favGameUpdate$ = this.favGameUpdated.asObservable();

  broadcastFavGameUpdate(favGameUpdateStatus: string) {
    this.favGameUpdated.next(favGameUpdateStatus);
  }

  private openRegistration = new Subject<string>();

  openRegistration$ = this.openRegistration.asObservable();

  broadcastOpenRegistration(openRegistrationStatus: string) {
    this.openRegistration.next(openRegistrationStatus);
  }

  private openCashier = new Subject<string>();

  openCashier$ = this.openCashier.asObservable();

  broadcastOpenCashier(openCashierStatus: string) {
    this.openCashier.next(openCashierStatus);
  }

  private getGametypes = new Subject<string>();

  getGametypes$ = this.getGametypes.asObservable();

  broadcastGetGametypes(getGametypesStatus: string) {
    this.getGametypes.next(getGametypesStatus);
  }

  private openLogin = new Subject<string>();

  openLogin$ = this.openLogin.asObservable();

  broadcastOpenLogin(openLoginStatus: string) {
    this.openLogin.next(openLoginStatus);
  }

  private lastPlayedUpdated = new Subject<string>();

  lastPlayedUpdated$ = this.lastPlayedUpdated.asObservable();

  broadcastLastPlayedUpdated(lastPlayedUpdatedStatus: string) {
    this.lastPlayedUpdated.next(lastPlayedUpdatedStatus);
  }

  private hideheader = new Subject<string>();

  hideheader$ = this.hideheader.asObservable();

  broadcastHideheader(hideheaderStatus: string) {
    this.hideheader.next(hideheaderStatus);
  }

  private openGamePopUp = new Subject<string>();

  openGamePopUp$ = this.openGamePopUp.asObservable();

  broadcastOpenGamePopUp(openGamePopUpstatus: string) {
    this.openGamePopUp.next(openGamePopUpstatus);
  }

  private modalClose = new Subject<string>();

  modalClose$ = this.modalClose.asObservable();

  broadcastModalClose(modalCloseId: string) {
    this.modalClose.next(modalCloseId);
  }

  private eligibleBonus = new Subject<any>();

  eligibleBonus$ = this.eligibleBonus.asObservable();

  broadcastEligibleBonus(eligibleBonusData: any) {
    this.eligibleBonus.next(eligibleBonusData);
  }

  private staticPageUrl = new Subject<any>();

  staticPageUrl$ = this.staticPageUrl.asObservable();

  broadcastStaticPageUrl(staticPageUrlData: any) {
    this.staticPageUrl.next(staticPageUrlData);
  }

  private statusCallDone = new Subject<any>();

  statusCallDone$ = this.statusCallDone.asObservable();

  broadcastStatusCallDone(statusCallDoneData: any) {
    this.statusCallDone.next(statusCallDoneData);
  }

  private socketConnected= new Subject<any>();

  socketConnected$ = this.socketConnected.asObservable();

  broadcastSocketConnected(socketConnectedData: any) {
    this.socketConnected.next(socketConnectedData);
  }

  private winEvent= new Subject<any>();

  winEvent$ = this.winEvent.asObservable();

  broadcastWinEvent(winEventData: any) {
    this.winEvent.next(winEventData);
  }

  private SSOError= new Subject<any>();

  SSOError$ = this.SSOError.asObservable();

  broadcastSSOError(SSOErrorData: any) {
    this.SSOError.next(SSOErrorData);
  }

  private showFavouriteGames= new Subject<any>();

  showFavouriteGames$ = this.showFavouriteGames.asObservable();

  broadcastShowFavouriteGames(showFavouriteGamesData: any) {
    this.showFavouriteGames.next(showFavouriteGamesData);
  }

  private cashierTransactionSuccess= new Subject<any>();

  cashierTransactionSuccess$ = this.cashierTransactionSuccess.asObservable();

  broadcastCashierTransactionSuccess(cashierTransactionSuccessData: any) {
    this.cashierTransactionSuccess.next(cashierTransactionSuccessData);
  }

  private getBannerAndPromo= new Subject<any>();

  getBannerAndPromo$ = this.getBannerAndPromo.asObservable();

  broadcastGetBannerAndPromo(getBannerAndPromoData: any) {
    this.getBannerAndPromo.next(getBannerAndPromoData);
  }

}
