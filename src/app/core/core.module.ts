import { NgModule } from '@angular/core';
import { AppHeaderComponent } from './app-header/app-header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginSignupModule } from '../login-signup/login-signup.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SettingsModule } from '../settings/settings.module';
import { CashierModule } from '../cashier/cashier.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared-modules/shared.module';
import { StaticPagesModule } from '../static-pages/static-pages.module';

@NgModule({
  imports: [
    RouterModule,
    LoginSignupModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    SettingsModule,
    CashierModule,
    SharedModule.forRoot(),
    StaticPagesModule.forRoot()
  ],
  declarations: [
    AppHeaderComponent,
    FooterComponent
  ],
  exports:[AppHeaderComponent,FooterComponent]
})
export class CoreModule { }
