import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRouterModule } from './app-router/app-router.module';
import { CoreModule } from './core/core.module';
import { HomeModule } from './home/home.module';
import { HttpModule } from '@angular/http';
import { SharedModule } from './shared-modules/shared.module';
import { CountryBlockModule } from './country-block/country-block.module';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

import { AjaxService } from './services/ajax.service';
import { BannerService } from './services/banner.service';
import { GameService } from './services/game.service';
import { UserDetailsService } from './services/user-details.service';
import { Utility } from './utility/utility';
import { AppDetailsService } from './services/app-details.service';
import { EmitterService } from './services/emitter.service';
import { CustomValidators } from './utility/custom-validator';
import { FixedFooterGamesComponent } from './fixed-footer-games/fixed-footer-games.component';
import { PromotionsComponent } from './promotions/promotions.component';
import { PromotionsService } from './services/promotions.service';
import { PromotionContentComponent } from './promotions/promotion-content/promotion-content.component';
import { CashierModule } from './cashier/cashier.module';
import { StaticPagesModule } from './static-pages/static-pages.module';
import { SocketService } from './shared-modules/services/socket.service';

@NgModule({
  declarations: [
    AppComponent,
    FixedFooterGamesComponent,
    PromotionsComponent,
    PromotionContentComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpModule,
    SharedModule.forRoot(),
    StaticPagesModule.forRoot(),
    AppRouterModule,
    CoreModule,
    HomeModule,
    CountryBlockModule,
    CashierModule
  ],
  providers: [
    Utility,
    AjaxService,
    GameService,
    BannerService,
    UserDetailsService,
    AppDetailsService,
    CustomValidators,
    EmitterService,
    PromotionsService,
    SocketService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
