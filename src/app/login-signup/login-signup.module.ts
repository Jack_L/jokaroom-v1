import { NgModule } from '@angular/core';
import { LoginSignupComponent } from './login-signup.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LoginMobilePopupComponent } from './login-mobile-popup/login-mobile-popup.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SharedModule } from '../shared-modules/shared.module';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule.forRoot()
  ],
  declarations: [
    LoginSignupComponent,
    LoginComponent,
    SignupComponent,
    LoginMobilePopupComponent,
    ResetPasswordComponent
  ],
  exports: [
    LoginSignupComponent,
    ResetPasswordComponent
  ]
})
export class LoginSignupModule { }
