import { Component, OnInit, ViewEncapsulation, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Utility } from '../../utility/utility';
import { AjaxService } from '../../services/ajax.service';
import { AppDetailsService } from '../../services/app-details.service';
import { CustomValidators } from '../../utility/custom-validator';
import { EmitterService } from '../../services/emitter.service';
import { FormValidationComponent } from '../../shared-modules/form-validation/form-validation.component';
import { GDPRCountries } from '../../utility/config';

import * as $ from 'jquery';
import 'intl-tel-input';
import * as _ from 'underscore';
import 'bootstrap-select';

//(keydown)="()"

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SignupComponent extends FormValidationComponent implements OnInit {
  @Input() callingFrom;
  availableCountries;
  availableCurrency;
  ipCountry;
  focusedElement;
  progressBarWidth;
  activeForm;
  validControls:any = [];
  stepOneWidthCalcControls =['email','nickname','password','phone'];
  stepTwoWidthCalcControls =['firstName','lastName','bDay','bMonth','bYear'];
  stepThreeWidthCalcControls =['address1','city','zip','currency','country'];
  @Output() loginComplete = new EventEmitter<string>();
  isStepOne: boolean = false;
  isStepTwo: boolean = false;
  isStepThree: boolean = false;
  yearRange = [];
  viewPassword:boolean = false;
  openRegSubs;
  isGDPR;
  emailAndMobileSubscribed = false;
  serverError;
  registrationErrorResponse;
  signUpStepOne = this.formBuilder.group({
    'email': ['', [this.CustomValidators.validateUniqueness("txtEmail",this.ajaxService,true)]],
    'nickname': ['', [this.CustomValidators.validateUniqueness("txtNickname",this.ajaxService,true)]],
    'password': ['', [this.CustomValidators.reqMin(4)]],
    'areaCode': ['',this.CustomValidators.required],
    'areaCountryCode': ['',this.CustomValidators.required],
    'phone': ['', [this.CustomValidators.phoneNumberValidator]]
  });
  // signUpStepOne = this.formBuilder.group({
  //   'email': [''],
  //   'nickname': [''],
  //   'password': [''],
  //   'areaCode': ['',this.CustomValidators.required],
  //   'areaCountryCode': ['',this.CustomValidators.required],
  //   'phone': ['']
  // });
  signUpStepTwo = this.formBuilder.group({
    'firstName': ['', [this.CustomValidators.validName(2,30)]],
    'lastName':  ['', [this.CustomValidators.validName(2,30)]],
    'gender': ['M', [this.CustomValidators.required]],
      'bDay': ['', [this.CustomValidators.required]],
      'bMonth': ['', [this.CustomValidators.required]],
      'bYear': ['', [this.CustomValidators.required]]
  }, {validator:this.CustomValidators.dobValidator});

  signUpStepThree = this.formBuilder.group({
    'address1': ['', [this.CustomValidators.addressValidator(false)]],
    'city': ['', [this.CustomValidators.validName(2,50)]],
    'zip': ['', [this.CustomValidators.validAlphaNumeric(2,9)]],
    'country': ['', [this.CustomValidators.required]],
    'state': ['-NA-', [this.CustomValidators.required]],
    'currency': ['', [this.CustomValidators.required]],
    'emailAndMobileSubscribed':['fasle']
  });
  constructor(
    private utils: Utility,
    private ajaxService: AjaxService,
    private formBuilder: FormBuilder,
    private appDetails:AppDetailsService,
    private CustomValidators:CustomValidators,
    private emitterService:EmitterService
  ) {
    super(utils);
    this.openRegSubs = this.emitterService.openRegistration$.subscribe(
      command =>{
        if(command == 'Open' && !this.callingFrom){
          this.openSignupModal('signUpModal-',false);
        }
      }
    )
  }

  ngOnInit() {
  }

  ngAfterViewInit() {}

  getProgressBarWidth(){
    let width;
    let widthCalcControl=[];
    if(this.activeForm){
      if(this.activeForm == this.signUpStepOne){
        width = 0;
        widthCalcControl = this.stepOneWidthCalcControls;
      }else if(this.activeForm == this.signUpStepTwo){
        width = 29.2;
        widthCalcControl = this.stepTwoWidthCalcControls;
      }else if(this.activeForm == this.signUpStepThree){
        width = 65.7;
        widthCalcControl = this.stepThreeWidthCalcControls;
      }
      let validControls:any = _.intersection(this.getValidFormContols(this.activeForm,[]),widthCalcControl);
      width += 7.3*validControls.length;
    }

    return width+"%";
  }

  getValidFormContols(formGroup: FormGroup,validControls){
      Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl && control.valid && field != this.focusedElement) {
          validControls.push(field)
        } else if (control instanceof FormGroup) {
          this.getValidFormContols(control,validControls);
        }
      });
      return validControls;
  }

  moveToPrevStep(stepNumber){
    console.log(stepNumber);
    if(this.isStepTwo || stepNumber == "error-step-one"){
      this.openSignupModal("",stepNumber ? stepNumber : true);
    }else if(this.isStepThree || stepNumber == "error-step-two"){
      this.showSecondStep(stepNumber ? stepNumber : true);
    }
  }



  openSignupModal(modalId,isPrevStep) {
    isPrevStep ? '' : this.utils.closeAllOpenModel();
    this.isStepTwo = false;
    this.isStepThree = false;
    this.isStepOne = true;
    this.serverError = undefined;
    let avCountryData = this.appDetails.getAvailableCountries();
    let avCurrencyData = this.appDetails.getAvailableCurrencies();
    if( avCountryData && avCurrencyData ){
      this.setFirstStepReqData(avCountryData,avCurrencyData,isPrevStep)
    }else{
      Promise
        .all([
          Promise.resolve(this.ajaxService.getCountries()),
          Promise.resolve(this.ajaxService.getEnabledCuurencies())
        ])
        .then(
          countryAndCurrencyData => {
            this.appDetails.setAvailableCountries(countryAndCurrencyData[0]);
            this.appDetails.setAvailableCurrencies(countryAndCurrencyData[1]);
            let countryData = countryAndCurrencyData[0];
            let currencyData = countryAndCurrencyData[1];
            this.setFirstStepReqData(countryData,currencyData,isPrevStep)
          },
          SystemError => {

          }
        );
    }
    isPrevStep?'':this.openModal(modalId,true);
    this.activeForm = this.signUpStepOne;
  }

  setFirstStepReqData(countryData,currencyData,isPrevStep){
    var self = this;
    this.yearRange = this.yearRange.length > 0 ? this.yearRange : _.sortBy(_.range(1900, new Date().getFullYear() - 17), function(num) { return -num; });
    if (countryData && countryData["countrylist"] && !isPrevStep) {
      this.ipCountry = countryData["country"];
      this.availableCountries = countryData["countrylist"];
      this.signUpStepOne.controls["areaCode"].value ? this.signUpStepOne.controls["areaCode"].value : this.signUpStepOne.controls["areaCode"].setValue(countryData["areaCode"]);
      this.signUpStepOne.controls["areaCountryCode"].value ? this.signUpStepOne.controls["areaCountryCode"].value : this.signUpStepOne.controls["areaCountryCode"].setValue(countryData["country"]);
    }
    if (currencyData && currencyData["currency"] && !isPrevStep) {
      this.availableCurrency = currencyData["currency"];
    }
    setTimeout(function() {
      self.utils.setPhoneNumberDropdown("phoneNumberFlag-"+self.callingFrom, isPrevStep && self.signUpStepOne.controls["areaCountryCode"].value? self.signUpStepOne.controls["areaCountryCode"].value :countryData["country"], [],self.signUpStepOne);
    });
  }

  moveToSecondStep() {
    if (this.signUpStepOne.valid) {
      this.showSecondStep('');
    } else {
      this.utils.validateAllFormFields(this.signUpStepOne);
    }
  }

  showSecondStep(isComingFromError){
    this.isStepOne = false;
    this.isStepThree = false;
    this.isStepTwo = true;
    this.serverError = undefined;
    setTimeout(function() {
      $('select').selectpicker({});
    }, 10);
    this.activeForm = this.signUpStepTwo;
    isComingFromError ? this.setRegistrationFormErrors(this.registrationErrorResponse):'';
  }

  moveToThirdStep() {
    if (this.signUpStepTwo.valid) {
      this.isStepOne = false;
      this.isStepTwo = false;
      this.isStepThree = true;
      this.serverError = undefined;
      setTimeout(function() {
        $('select').selectpicker('refresh');
      })
      if(this.signUpStepOne.controls["areaCountryCode"].value){
        this.signUpStepThree.controls["country"].setValue(this.signUpStepOne.controls["areaCountryCode"].value);
        this.checkGDPR();
      }
      this.activeForm = this.signUpStepThree;
    } else {
      this.utils.validateAllFormFields(this.signUpStepTwo);
    }
  }

  registerUser(buttonId) {
    this.serverError = undefined;
    if (this.signUpStepThree.valid) {
      this.utils.disableButton(buttonId,"please wait...",true)
      let signUpData = this.utils.formControlToParams(this.signUpStepOne, {});
      signUpData = this.utils.formControlToParams(this.signUpStepTwo, signUpData);
      signUpData = this.utils.formControlToParams(this.signUpStepThree, signUpData);
      signUpData["confirmPassword"] = signUpData["password"];
      signUpData["birthDate"] = signUpData['bYear']+'-'+signUpData['bMonth']+'-'+signUpData['bDay'];
      let affId = this.utils.getAffIdCookie("affIdCookie");
      let trackerId = this.utils.getAffIdCookie("trackerIdCookie");
      if(affId){
        signUpData['affiliateId'] = affId;
      }
      if(trackerId){
        signUpData['trackerId'] = trackerId;
      }
      if(this.emailAndMobileSubscribed) signUpData["emailAndMobileSubscribed"] = false;
      else signUpData["emailAndMobileSubscribed"] = true;
      Promise.resolve(this.ajaxService.doRegistration(signUpData))
      .then(
        registrationResponse =>{
          if(registrationResponse && registrationResponse["success"] === "true"){
             this.utils.deleteCookie();
            let logincredentials = {
              'txtEmail':signUpData["email"],
              'txtPassword':signUpData["password"]
            }
            Promise.resolve(this.ajaxService.doLogin(logincredentials))
            .then(
              data=>{
                let emitData;
                if(data  && data["success"] === "true") {
                  emitData = {
                    message: "Login Success After Registration/"+buttonId,
                    rvpSessionId : data["rvpSessionId"],
                    socketUrl:data["riverplayUrl"],
                    partnerID:data["user"] ? data["user"]["partnerId"] : 'jokaroom'
                  }
                }else {
                  emitData = {
                    message: "Login Failed After Successfull Registration/"+buttonId,
                    rvpSessionId : ""
                  }
                }
                this.loginComplete.emit(emitData);


              },
              SystemError =>{

              }
            )
          }else if(registrationResponse && registrationResponse["errors"]){
            this.utils.enableButton(buttonId,"DONE");
            this.registrationErrorResponse = registrationResponse;
            this.setRegistrationFormErrors(this.registrationErrorResponse)
          }else{
            this.serverError = "Something went wrong. Please try again!!";
            this.utils.enableButton(buttonId,"DONE");
          }
        },
        SystemError =>{}
      )
    } else {
      this.utils.validateAllFormFields(this.signUpStepThree);
    }
  }

  setRegistrationFormErrors(regData){
    // /signUpStepOne
  //signUpStepTwo
  //signUpStepThree
  let serverError;
    for(var key in regData["errors"]){
      switch(key){
        case 'firstName':
        this.signUpStepTwo.controls["firstName"].setErrors({'message':this.setServerError(key,regData["errors"][key])});
        break;
        case 'lastName':
        this.signUpStepTwo.controls["lastName"].setErrors({'message':this.setServerError(key,regData["errors"][key])});
        break;
        case 'nickname':
        this.signUpStepOne.controls["nickname"].setErrors({'message':this.setServerError(key,regData["errors"][key])});
        break;
        case 'password':
        case 'confirmPassword':
        this.signUpStepOne.controls["password"].setErrors({'message':this.setServerError(key,regData["errors"][key])})
        break;
        case 'email':
        serverError = this.setServerError(key,regData["errors"][key]);
        this.signUpStepOne.controls["email"].setErrors({'message':this.setServerError(key,regData["errors"][key])});
        break;
        case 'areaCode':
        case 'internationalPhone':
        case 'phone':
        this.signUpStepOne.controls["phone"].setErrors({'message':this.setServerError(key,regData["errors"][key])});
        break;
        case 'currency':
        this.signUpStepThree.controls["currency"].setErrors({'message':this.setServerError(key,regData["errors"][key])});
        break;
        default:
        serverError = regData["errors"][key]["lblWarning"]
        //this.registrationForm.controls["firstName"].setErrors({'message':regData["errors"][key]["lblWarning"]});
        break;
      }
    }
    if(!this.signUpStepOne.valid){
      this.moveToPrevStep("error-step-one");
    }else if(!this.signUpStepTwo.valid){
      this.moveToPrevStep("error-step-two");
    }
    this.serverError = serverError;
  }

  setServerError(key,errorMessage){
    if(errorMessage.includes("(Anonymous function)")){
      return "Invalid "+key;
    }else{
      return errorMessage;
    }

  }

  checkGDPR(){
    if(_.contains(GDPRCountries,this.signUpStepThree.controls["country"].value)){
    //if(GDPRCountries.includes(this.signUpStepThree.controls["country"].value)){
      this.isGDPR = true;
    }else{
      this.isGDPR = false;

    }
    //if()
  }
  broadcastOpenTC(){
    this.emitterService.broadcastStaticPageUrl("terms-and-conditions");
  }
  ngOnDestroy(){
    this.utils.closeAllOpenModel();
    $("body>#signUpModal-"+this.callingFrom).remove();
  }

}
