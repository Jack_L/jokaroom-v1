import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login-signup',
  templateUrl: './login-signup.component.html',
  styleUrls: ['./login-signup.component.scss']
})
export class LoginSignupComponent implements OnInit {

  @Output() loginComplete = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  emitLoginComplete(message){
    this.loginComplete.emit(message);
  }
}
