import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AjaxService } from '../../services/ajax.service';
import { Utility } from '../../utility/utility';
import { EmitterService } from '../../services/emitter.service';

import * as $ from 'jquery';
import * as _ from 'underscore';

import { CustomValidators } from '../../utility/custom-validator';
import { FormValidationComponent } from '../../shared-modules/form-validation/form-validation.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent extends FormValidationComponent implements OnInit {

  closeFieldsTimeout;
  serverError;
  forgetPasswordServerError;
  focusedElement;
  viewPassword = true;
  loginForm = this.formBuilder.group({
    'txtEmail': ['', [this.customValidators.required]],
		'txtPassword': ['', [this.customValidators.required]]
	});
  forgotPasswordForm = this.formBuilder.group({
    'email': ['', [this.customValidators.validateUniqueness("txtEmail",this.ajaxService,false)]],
  });
  @Output() loginComplete = new EventEmitter<any>();
  constructor(
    private formBuilder:FormBuilder,
    private ajaxService:AjaxService,
    private utils:Utility,
    private customValidators:CustomValidators,
    private emitterService:EmitterService
  ) {
    super(utils);
    this.emitterService.openLogin$.subscribe(
      openLoginModalData => {
        if(openLoginModalData == "login"){
          this.openModal("loginModal",true);
        }else if(openLoginModalData == "forgot-password"){
          this.openForgotPassword()
        }
      }
    )
  }

  ngOnInit() {
    let self= this;
    $(document).on("mouseout", ".loginFormContainer", function () {
      self.closeFieldsTimeout = setTimeout(function(){
        if(!self.loginForm.controls["txtEmail"].value && !self.loginForm.controls["txtPassword"].value){
          self.utils.closeLoginFields();
        }
      },3000)
    }).on("mouseover", ".loginFormContainer", function () {
      clearTimeout(self.closeFieldsTimeout);
    })
  }

  openForgotPassword(){
    this.utils.closeLoginFields();
    this.openModal("loginModal",true);
    $(".modal-body .loginBoxModal").addClass('hide');
    $(".modal-body .forgotPwdBox").removeClass('hide');
  }

  forgotPassword(buttonId){
		this.forgetPasswordServerError= "";
		if (this.forgotPasswordForm.valid) {
      this.utils.disableButton(buttonId,"please wait...",true);
			let emailDetails = this.utils.formControlToParams(this.forgotPasswordForm,{});
			Promise.resolve(this.ajaxService.forgotPassword(emailDetails))
			.then(forgotPasswordResp => {
				if(forgotPasswordResp && forgotPasswordResp["success"]){
          this.forgetPasswordServerError="RESET_PASSWORD_SUCCESS";
				}else{
          if(forgotPasswordResp && forgotPasswordResp["error"]){
            this.forgetPasswordServerError = forgotPasswordResp["error"];
          }else{
            this.forgetPasswordServerError = "Something went wrong. Please try again later."
          }
				}
        this.utils.enableButton(buttonId,"RECOVER PASSWORD");
			},
			SystemError => {
				this.forgetPasswordServerError = SystemError;
        this.utils.enableButton(buttonId,"RECOVER PASSWORD");
			});
		}else{
      this.utils.validateAllFormFields(this.forgotPasswordForm);
    }
	}

  stopBodyClickPropogation($event){
    $event.stopPropagation();
  }

  login(buttonId){
    var self = this;
    this.serverError = "";
    if($(".loginForm").hasClass('formFieldShow') || buttonId == "loginPopupButton"){
      if(this.loginForm.valid){
        this.utils.disableButton(buttonId,"",true);
        let credentials = this.utils.formControlToParams(this.loginForm,{});
        Promise.resolve(this.ajaxService.doLogin(credentials))
  			.then(loginData => {
  				if(loginData && loginData["success"] === "true"){
            let emitData = {
              message: "LoginSuccess/"+buttonId,
              rvpSessionId : loginData["rvpSessionId"],
              socketUrl:loginData["riverplayUrl"],
              partnerID:loginData["user"] ? loginData["user"]["partnerId"] : 'jokaroom'
            }
  					this.loginComplete.emit(emitData);
            $(".loginForm").removeClass('formFieldShow');
            $('#loginButton').removeClass('active');
  				}else{
            this.utils.enableButton(buttonId,"");
            if(loginData && loginData["errors"]){
  						if(loginData["errors"]["txtEmail"]){
  							this.serverError = loginData["errors"]["txtEmail"];
  						}else if(loginData["errors"]["remoteError"]){
  							this.serverError = loginData["errors"]["remoteError"];
  						}else if(loginData["errors"]["accountLocked"]){
                let errorMessage = loginData["errors"]["accountLocked"].split(":")[0];
  							this.serverError = errorMessage + new Date(loginData["errors"]["accountLocked"].substr(loginData["errors"]["accountLocked"].indexOf(":")) + " +0200");
  						}else{
  							this.serverError = "Something went wrong. Please Try again Later";
  						}
  					}else{
              this.serverError = "Something went wrong. Please Try again Later";
            }
  				}
  			},
  			SystemError => {
          this.utils.enableButton(buttonId,"");
  				this.serverError = "Something went wrong. Please Try again Later";
  			});
      }else{
        this.utils.validateAllFormFields(this.loginForm);
        let errorArray =  this.utils.getAllFormFieldsErrors(this.loginForm,[]);

        _.each(errorArray,function(errors){
          if(errors.key == "txtEmail"){
            self.serverError += "Email or Username is required.<br/>"
          }
          if(errors.key == "txtPassword"){
            self.serverError += "Password required."
          }
        })
        //this.serverError = "Please check if the details entered are valid."
      }
    }else{
      $(".loginForm").addClass('formFieldShow');
      $('.tooltip-login').css("display", "block");
      $('#loginButton').addClass('active');
    }
  }

  openSignUpModal(){
    this.closeModal("loginModal",true,true);
    this.openModal("signUpModal",true)
  }

  closeLoginModal(){
    $(".modal-body .loginBoxModal").removeClass('hide');
    $(".modal-body .forgotPwdBox").addClass('hide');
    this.closeModal("loginModal",true,true);
  }

  emitLoginComplete(message){
    this.loginComplete.emit(message);
  }



}
