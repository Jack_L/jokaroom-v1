import { Component, OnInit, ViewEncapsulation,HostListener } from '@angular/core';
import { AjaxService } from '../services/ajax.service';
import { environment } from '../../environments/environment';
import { PromotionsService } from '../services/promotions.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import * as _ from 'underscore';
import { EmitterService } from '../services/emitter.service';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PromotionsComponent implements OnInit {
  promotions;
  imagePath = "/uploads/promotion/";
  promotionImagePath ;
  promoH5Array = [];
  promoPArray = [];
  loginSubs;
  logoutSubs;
  cashierTransactionSuccess
  constructor(
    private ajaxService:AjaxService,
    private promotionsService:PromotionsService,
    private router:Router,
    private emitterService:EmitterService
  ) {

    this.loginSubs = this.emitterService.loginComplete$.subscribe(
      loginStatus => {
        if (loginStatus == "LoginCompleted") {
          this.getPromotions(true);
        }
      }
    )

    this.logoutSubs = this.emitterService.logoutComplete$.subscribe(
      logoutStatus => {
        if (logoutStatus == "LogoutCompleted") {
          this.getPromotions(true);
        }
      }
    )

    this.cashierTransactionSuccess = this.emitterService.getBannerAndPromo$.subscribe(
      transactionSuccessData => {
        if (transactionSuccessData == "TimeOutOver") {
          var self = this;
            self.getPromotions(true);
        }
      }
    )

  }

  @HostListener('window:resize') onResize() {
    // guard against resize before view is rendered
    this.iterateCard(false);
  }

  ngOnInit() {
    this.promotionImagePath = environment.siteUrl+this.imagePath;
    this.getPromotions(false);
  }

  getPromotions(isForce){
    var self = this;
    this.promotions = undefined;
    Promise.resolve(this.promotionsService.getPromotionsData(isForce))
    .then(
      promotionsData =>{
        _.each(promotionsData,function(promotion){
          promotion.snippet = promotion["snippet"].replace(new RegExp('{{site_url}}','g'), environment.siteUrl);
        })
        this.promotions = promotionsData;
        setTimeout(function(){
          self.iterateCard(true)
        })
      },
      SystemError=>{
        this.promotions = [];
      }
    )
  }

  iterateCard(force){
    var self = this;
    $(".promo-card").each(function(key,card){
      if(force){
        self.promoH5Array.push($(card).children(".promo-card--intro-text").children("h5").text())
        self.promoPArray.push($(card).children(".promo-card--intro-text").children("p").text())
      }
      self.truncateTexts($(card).children(".promo-card--intro-text").children("h5"),key,self.promoH5Array);
      self.truncateTexts($(card).children(".promo-card--intro-text").children("p"),key,self.promoPArray);
    })


    // Select and loop the container element of the elements you want to equalise
    // $('.promo-card-row').each(function(){

    //    var highestBox = 0;

    //    $('.promo-card-col', this).each(function(){

    //      if($(this).height() > highestBox) {
    //       highestBox = $(this).height();
    //     }

    //   });

    //    $('.promo-card-col',this).height(highestBox);

    // });
  }

  truncateTexts(card,index,array){

    var maxWidth = parseInt(card.css("width"), 10)-15;
    var maxLines = 2;

    // ----------------------------------------------



    var startIndex = 0;
    var ellipsisText = "";

    // Get font style and size for source text
    var $objCss = card.css("font");

    // Split source text into array on spaces.
    var arr = array[index].split(" ");

    // Get width of space
    var spaceWidth = this.getTextWidth(" ", $objCss);

    // Get width of ellipsis
    var ellipsisWidth = this.getTextWidth(" ...", $objCss);

    // For each line
    for (var o = 0; o < maxLines; o++) {
        var newLine = "";
        var newLineWidth = 0;

        ellipsisWidth = (o + 1 == maxLines ? ellipsisWidth : 0);

        // For each word in the array
        for (var i = startIndex; i < arr.length; i++) {
            // Get width of each word.
            var wordWidth = this.getTextWidth(arr[i], $objCss);

            // Append word to newLine
            // Don't allow newLine to be wider than maxWidth
            if ((wordWidth + spaceWidth + newLineWidth + ellipsisWidth) < maxWidth) {
                newLineWidth += (wordWidth + spaceWidth);
                newLine += arr[i] + " ";
                startIndex = i + 1;
                // TODO: Handle words wider than maxWidth
            }
            else {
                break;
            }
        }
        //console.log(newLineWidth,maxWidth,startIndex, arr.length);
        ellipsisText += (newLine + (o + 1 == maxLines && startIndex < arr.length ? " ..." : startIndex >= arr.length ? "" : "<br/>"));
    }

    // Show ellipsis text
    card.html(ellipsisText);
  }

  getTextWidth(text, font) {
      var canvas = document.createElement("canvas")
      var context = canvas.getContext("2d");
      context.font = font;
      var metrics = context.measureText(text);
      return metrics.width;
  }

  loadPromoContent(index){
      this.router.navigate(["/en/"+index]);
  }

  ngOnDestroy(){
    this.loginSubs.unsubscribe();
    this.logoutSubs.unsubscribe();
    this.cashierTransactionSuccess.unsubscribe();
  }

}
