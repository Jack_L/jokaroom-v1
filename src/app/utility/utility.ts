import { Injectable, Inject } from '@angular/core';
import { FormControl, FormGroup} from '@angular/forms';
import { DOCUMENT } from '@angular/common';
import { environment } from '../../environments/environment';
import { EmitterService } from '../services/emitter.service'
import { Router } from '@angular/router';
import { UserDetailsService }  from '../services/user-details.service';
import { phoneNumberExclusion } from './config';
import { availableCountries } from './config';
declare var $:any;
import 'intl-tel-input';
import * as _ from 'underscore';
@Injectable()
export class Utility {

  affIdCookie = "_affId"+environment.websiteCode;
  trackerIdCookie = "_trackerId"+environment.websiteCode;
  constructor(
      @Inject(DOCUMENT) private doc: any,
      private emitterService:EmitterService,
      private router:Router,
      private userDetailsService:UserDetailsService
  ) {
    this.userDetailsService.userProfileUpdated$.subscribe(
      profileUpdateMessage => {
        if(profileUpdateMessage == "UserProfileUpdated"){
          this.elementsChange();
        }
      }
    )
  }


  setAffIdTOCookie(cookieData,cookieName){
    var date = new Date();
    var newdate = new Date(date);
    date.setDate(date.getDate() + 30);
    var expires = "expires="+date.toUTCString();
    document.cookie = this[cookieName] + "=" + cookieData + ";" + expires + ";path=/";
  }

  getAffIdCookie(cookieName){
    var name = this[cookieName] + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
  }


  deleteCookie(){
    document.cookie = this.affIdCookie + "="  + "; expires=Thu, 01 Jan 1970 00:00:01 GMT ;path=/";
    document.cookie = this.trackerIdCookie + "="  + "; expires=Thu, 01 Jan 1970 00:00:01 GMT ;path=/";
  }

  loadPaymentIqEncryptor(){
    const s = this.doc.createElement('script');
    s.type = 'text/javascript';
    s.src = environment.paymentIqUrl+'/api/viq/jscardencrypter/'+environment.paymentIqMID;
    $("head").append(s);
  }

  formControlToParams(values,data){
    Object.keys(values.controls).forEach(field => {
      const control = values.get(field);
      if (control instanceof FormControl) {
        data[field] = control.value;
      } else if (control instanceof FormGroup) {
        this.formControlToParams(control,data);
      }
    });
    return data;
  }

  openModal(modalId,isSlideRight): void{
    var self = this;
    if(isSlideRight){
      $('#'+modalId).on('show.bs.modal', function (e) {
        $('#'+modalId).addClass("slideInRight").removeClass("slideOutRight");
      })
    }
    $('#'+modalId).on('hidden.bs.modal',function(e){
      self.emitterService.broadcastModalClose(modalId);
      $("body").removeClass("blur-bg")
    })
    $('#'+modalId).modal();
    $('#'+modalId).appendTo("body");
  }

  closeModal(modalId,isSlideLeft,forceClose):void {
    if(isSlideLeft){
      $('#'+modalId).removeClass("slideInRight").addClass("slideOutRight");
    }
    if(forceClose){
      $('#'+modalId).modal("hide");
    }else{
      setTimeout(function(){
        $('#'+modalId).modal("hide");
      },250);
    }
    $("body").removeClass("blur-bg")
  }

  closeAllOpenModel(){
    $(".modal").modal("hide");
  }

  setPhoneNumberDropdown(inputId,initialSelected,excludedCountries,form,onlyCountries?){
    initialSelected = initialSelected &&  _.contains(availableCountries,initialSelected) ? initialSelected : "AF";
    excludedCountries = excludedCountries.length > 0 ? excludedCountries : phoneNumberExclusion;
    onlyCountries = onlyCountries && onlyCountries.length > 0 ? onlyCountries : availableCountries;
    $("#"+inputId).intlTelInput({
      onlyCountries:onlyCountries,
      initialCountry: initialSelected,
      nationalMode: false,
      preferredCountries: [],
      autoPlaceholder: false,
      separateDialCode: true
    });
    $("#"+inputId).on("countrychange" ,function(e, countryData) {
      if(inputId == "mobileNumberFlag"){
        form.controls["mobileAreaCode"].setValue(countryData.dialCode);
        form.controls["mobileAreaCountryCode"].setValue(countryData.iso2.toUpperCase());
      }else{
        form.controls["areaCode"].setValue(countryData.dialCode);
        form.controls["areaCountryCode"].setValue(countryData.iso2.toUpperCase());
      }
    });;
  }

  validateAllFormFields(formGroup: FormGroup){
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  getAllFormFieldsErrors(formGroup: FormGroup,errorArray){
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl && control.errors) {
        errorArray.push({'key':field,'message':control.errors.message})
      } else if (control instanceof FormGroup) {
        this.getAllFormFieldsErrors(control,errorArray);
      }
    });
    return errorArray;
  }

  shouldShowErrors(controlFormNField,formName): boolean {
    let control;
    control = formName.controls[controlFormNField];
    return control &&
    control.errors &&
    (control.dirty || control.touched);
  }

  isControlValid(controlFormNField,formName): boolean {
    let control = formName.controls[controlFormNField];
    return control && !control.errors && (control.dirty || control.touched);
  }

  getErrorMessage(controlFormNField,formName): string{
    let control;
    control = formName.controls[controlFormNField];
    return control.errors.message;
  }

  isUserLoggedIn():boolean{
    let user = JSON.parse(sessionStorage.getItem("user"));
    if(user){
      return true;
    }
    return false;
  }

  disableButton(buttonId,disabledText,loader){
    $("#"+buttonId).prop('disabled', true);
    if(disabledText){
      $("#"+buttonId+" span").text(disabledText);
    }
    if(loader){
      $("#"+buttonId).addClass("btn-progress");
    }
  }

  enableButton(buttonId,enableText){
    $("#"+buttonId).prop('disabled', false);
    if(enableText){
        $("#"+buttonId+" span").text(enableText);
    }
    $("#"+buttonId).removeClass("btn-progress");
  }

  creditCardValidation(ccNum): boolean{

    let arr = [0, 2, 4, 6, 8, 1, 3, 5, 7, 9];
    var
    len = ccNum.length,
    bit = 1,
    sum = 0,
    val;
    if(len >=13 && len <=19){
      while (len) {
        val = parseInt(ccNum.charAt(--len), 10);
        sum += (bit ^= 1) ? arr[val] : val;
      }
    }


    return sum && sum % 10 === 0;
  }

  closeFixedFooter($event) {
    $('.fixed-footer').removeClass('opened');
    $("#closeButton").removeClass('show').addClass('hide');
    $('ul.f-tabs li').removeClass('current');
    $('.f-tab-content').removeClass('current');
    if($event){
      $event.stopPropagation();
    }
  }

  closeLoginFields(){
    $(".loginForm").removeClass('formFieldShow');
    $('#loginButton').removeClass('active');
    $('.tooltip-login').css("display", "none");
  }

  hideAndShowQuickDeposit(buttonName){
    $(".btn.btn-primary.qd-btn.deposit").addClass("hide");
    $(".btn.btn-primary.qd-btn.TopUp").addClass("hide");
    $(".btn.btn-primary.qd-btn.ok").addClass("hide");
    $(".btn.btn-primary.qd-btn.retry").addClass("hide");
    $(".btn.btn-primary.qd-btn."+buttonName).removeClass("hide");
  }

  gameContentFixer(){
    var gameSearchBar = $('.game-searchbar'),
      gameFooter = $("#gamesFooter").offset(),
      windowVar = $(window).scrollTop(),
      staticHeader = $(".staticHeader").height(),
      bannerHeight = $(".cm-banner").height(),
      gameSectionSideMenu = $(".game-section-sidemenu").height(),
      fixedFooterHeight = $(".fixed-footer").height();
    if(gameSearchBar.hasClass('fix-search') && $("#gamesContainer").height && $("#gamesContainer").height() > gameSectionSideMenu){
      if(gameFooter.top - windowVar < staticHeader + gameSearchBar.height() + gameSectionSideMenu + 50){
        $(".game-section-sidemenu").removeClass("fix-bottom-side-nav");
        $(".game-section-sidemenu").addClass("fix-side-nav");
        $(".game-section-sidemenu").css('position','fixed');
        $(".game-section-sidemenu").css('top',gameFooter.top - windowVar - bannerHeight - gameSearchBar.height() - 50);
      }else{
        if($('.game-searchbar').hasClass('fix-search')){
          $(".game-section-sidemenu").addClass("fix-bottom-side-nav");
          $(".game-section-sidemenu").removeClass("fix-side-nav");
          $(".game-section-sidemenu").css('position','fixed');
          $(".game-section-sidemenu").css('top',parseInt(gameSearchBar.css('top'), 10)+gameSearchBar.height() + 20)

        }else{
          $(".game-section-sidemenu").removeClass("fix-bottom-side-nav");
          $(".game-section-sidemenu").removeClass("fix-side-nav");
          $(".game-section-sidemenu").css('top','');
          $(".game-section-sidemenu").css('position','');
        }
      }
    }else{
      $(".game-section-sidemenu").removeClass("fix-bottom-side-nav");
      $(".game-section-sidemenu").removeClass("fix-side-nav");
      $(".game-section-sidemenu").css('top','');
      $(".game-section-sidemenu").css('position','');
    }
  }

  gameHeaderFixer(){
    var sticky = $('.game-searchbar'),
    scroll = $(window).scrollTop();
    if (scroll >= $('.cm-banner').height()){
      this.setContainerSideNav();
      sticky.addClass('fix-search');
      let additionalPixel = 0;
      if(document.body.clientWidth < 991)
        additionalPixel = 2;
      sticky.css('top',$(".staticHeader").height()+additionalPixel +'px');
    }
    else{
      $('.games-container').css('margin-top','');
      $('.game-section-sidemenu').css('margin-top','');
      sticky.removeClass('fix-search');
      sticky.css('top','');
    }
  }

  setContainerSideNav(){
    $('.games-container').css('margin-top','50px');
    if(!$(".game-section-sidemenu").hasClass("fix-bottom-side-nav") && !$(".game-section-sidemenu").hasClass("fix-side-nav")){
      $('.game-section-sidemenu').css('margin-top','50px');
    }else{
      $('.game-section-sidemenu').css('margin-top','');
    }
  }

  cmsRouter(routerLink){
    let router = routerLink.toLowerCase();
    if( router == "register"){
      this.emitterService.broadcastOpenRegistration("Open");
    }else if(router == "login" || router == "forgot-password"){
      this.emitterService.broadcastOpenLogin(routerLink);
    }else if( router == "deposit" || router == "withdrawal" ){
      this.emitterService.broadcastOpenCashier(router);
    }else if( router == "getfreegames" || router == "getrealgames" ){
      this.emitterService.broadcastGetGametypes(routerLink);
    }else if(router.includes('faq') || router.includes('terms-and-conditions') || router.includes('promo-terms')){
      this.emitterService.broadcastStaticPageUrl(routerLink);
    }else{
      this.router.navigate([routerLink]);
      this.closeAllOpenModel();
    }
  }

  checkForExpand(leftMenuOpenend,e){
    var self  = this;
    $(".collapse.sub-menu").hide(200);
    $(".menu-icn i").removeClass("icon-minus").addClass("icon-plus");
    var $tagetData = $(e.target).data('target') ? $(e.target) : $(e.target).parent();
    if(leftMenuOpenend != $tagetData.data('target')){
      leftMenuOpenend = $tagetData.data('target');
      $("#"+$tagetData.data('target')).show(200);
      $($tagetData.children("i")).addClass("icon-minus").removeClass("icon-plus");
      //$(e.target+" i").toggleClass("icon-minus").toggleClass("icon-plus");
    }else{
      $($tagetData.children("i")).removeClass("icon-minus").addClass("icon-plus")
      leftMenuOpenend = undefined;
    }
    e.stopPropagation();
    e.preventDefault();
    return leftMenuOpenend;
  }

  setSEO(contents, isGame){
    if(!contents){
      contents={
        "metaTags":"Online Casino",
        "metaDescription":"JokaRoom has the best payout online casino games with the biggest rewards. Get $2000 + 75 Free Spins when you sign up for a real money online casino account.",
        "title":"Top Online Casino Real Money – $2000 Free + 75 Free Spins at JokaRoom",

      }
    }
    if(contents["metaTags"]){
      if($('meta[name=keywords]').length > 0){
        $('meta[name=keywords]').attr("content", contents["metaTags"]);
      }else{
        $('head').append('<meta name="keywords" content="'+contents["metaTags"]+'" />');
      }
    }
    if(contents["metaDescription"]){
      if($('meta[name=description]').length > 0){
        $('meta[name=description]').attr("content", contents["metaDescription"]);
      }else{
        $('head').append('<meta name="description" content="'+contents["metaDescription"]+'" />');
      }
    }
    if(contents["title"]){
      $(document).attr("title", contents["title"]);
    }
    if(isGame && contents["name"]){
      $(document).attr("title", contents["name"]);
    }
  }

  elementsChange(){
    if(this.isUserLoggedIn()){
      $(".not-logged-in").addClass("hide");
      $(".logged-in-all").removeClass("hide");
      $('.get-free-games').removeClass("hide");
      if(this.userDetailsService.getUserProfileDetails() && this.userDetailsService.getUserProfileDetails().userStatus == "real"){
        $(".logged-in-play").addClass("hide");
        $(".logged-in-real").removeClass("hide");
      }else{
        $(".logged-in-real").addClass("hide");
        $(".logged-in-play").removeClass("hide");

      }
    }else{
      $(".not-logged-in").removeClass("hide");
      $(".logged-in-all").addClass("hide");
      $(".logged-in-real").addClass("hide");
      $(".logged-in-play").addClass("hide");
    }
  }

}
