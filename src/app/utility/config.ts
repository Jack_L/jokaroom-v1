export const transactionTypeConfs = [
  "REAL_CASH_DEPOSIT",
  "REAL_CASH_WITHDRAW",
  "PLAYER_SUBSCRIPTION",
  "REAL_CASH_ADDITION_BY_CS",
  "REAL_CASH_REMOVAL_BY_CS",
  "NEGATIVE_BALANCE_ADDITION_FOR_WIN_REVERSE",
  "REAL_CASH_ADJUSTMENT_FOR_WIN_REVERSE_CAPTURE",
  "POSITIVE_ADJUSTMENT",
  "NEGATIVE_ADJUSTMENT",
  "NEGATIVE_BALANCE_ADDITION_FOR_FRAUD",
  "REAL_CASH_ADJUSTMENT_FOR_FRAUD_CAPTURE_BY_CS",
  "REAL_CASH_DEBIT_FOR_USER_INACTIVITY"
];

export const GDPRCountries = ['AL','AD','AT','BY','BE','BA','BG','HR','CY','CZ','DK','EE','FO','FI','FR','DE','GI','GR','HU','IS','IE','IM','IT','RS','LV','LI','LT','LU','MK','MT','MD','MC','ME','NL','NO','PL','PT','RO','RU','SM','RS','SK','SI','ES','SE','CH','UA','GB','VA','RS'];

export const currencyCodeMap =
{"AED": "د.إ",	"ANG": "ƒ",	"AOA": "AOA",	"ARS": "$",	"AUD": "A$",	"BAM": "KM",	"BBD": "$",	"BGN": "лв",	"BHD": "BD",	"BND": "$",	"BRL": "R$",	"CAD": "C$",	"CHF": "Fr",	"CLF": "UF",	"CLP": "$",	"CNY": "¥",	"COP": "$",	"CRC": "₡",	"CZK": "Kč",	"DKK": "kr",	"EEK": "KR",	"EGP": "E£",	"EUR": "€",	"FJD": "FJ$",	"GBP": "£",	"GTQ": "Q",	"HKD": "$",	"HRK": "kn",	"HUF": "Ft",	"IDR": "Rp",	"ILS": "₪",	"INR": "Rs",	"JOD": "د.ا",	"JPY": "¥",	"KES": "KSh",	"KRW": "₩",	"KWD": "KD",	"KYD": "$",	"LTL": "Lt",	"LVL": "Ls",	"MAD": "د.م",	"MVR": "Rf",	"MXN": "$",	"MYR": "RM",	"NGN": "₦",	"NOK": "kr",	"NZD": "$",	"OMR": "OMR",	"PEN": "S/.",	"PHP": "₱",	"PLN": "zł",	"QAR": "QAR",	"RON": "lei",	"RUB": "руб",	"SAR": "SAR",	"SEK": "kr",	"SGD": "$",	"THB": "฿",	"TRY": "TL",	"TTD": "$",	"TWD": "$",	"UAH": "₴",	"USD": "$",	"VEF": "Bs",	"VND": "₫",	"XCD": "$",	"ZAR": "R"}


export const availableCountries = ['AF','AX','AL','DZ','AS','AD','AO','AI','AG','AR','AM','AW','AU','AT','AZ','BS','BH','BD','BB','BY','BZ','BJ','BM','BT','BO','BA','BW','BR','BN','BG','BF','BI','KH','CM','CA','CV','KY','CF','TD','CL','CN','CX','CC','CO','KM','CG','CD','CK','CR','CI','HR','CU','DJ','DM','DO','EC','EG','SV','GQ','ER','EE','ET','FK','FO','FJ','FI','GA','GM','GE','DE','GH','GR','GD','GP','GU','GT','GG','GN','GW','GY','HT','HN','HK','IS','IN','ID','IR','IQ','IE','IM','IL','JM','JO','KZ','KE','KI','KP','KR','KW','KG','LA','LV','LB','LS','LR','LY','LI','LT','LU','MO','MK','MG','MW','MY','MV','ML','MT','MH','MQ','MR','MU','YT','MX','FM','MD','MC','MN','MS','MA','MZ','MM','NA','NR','NP','NL','NC','NZ','NI','NE','NG','NU','NF','NO','OM','PK','PW','PS','PA','PG','PY','PL','PT','PR','QA','RE','RU','RW','SH','KN','LC','PM','VC','BL','MF','WS','ST','SA','SN','RS','SC','SL','SG','SI','SB','SO','SS','LK','SD','SR','SZ','SE','CH','SY','TW','TJ','TZ','TH','TL','TG','TK','TO','TT','TN','TR','TM','TC','TV','UG','UA','AE','UY','UZ','VU','VE','SX','CW','ME'];

export const providerGameSortOrder = ["Quick Spin","IGTech","iSoftBet","Betsoft"]

export const phoneNumberExclusion = ['BE','IO','VG','BQ','CY','CZ','DK','FR','GF','PF','GL','HU','IT','JP','JE','XK','PE','PH','RO','SK','ZA','ES','SJ','GB','US','VA'];
