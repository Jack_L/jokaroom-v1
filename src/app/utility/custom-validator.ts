import { FormArray, FormControl, FormGroup, ValidationErrors, AbstractControl } from '@angular/forms';
import { Utility } from './utility';
import { AjaxService } from '../services/ajax.service';
import * as _ from 'underscore';

export class CustomValidators {
  static CHAZ = '\u4E00-\u9FCC\u3400-\u4DB5\uFA0E\uFA0F\uFA11\uFA13\uFA14\uFA1F\uFA21\uFA23\uFA24\uFA27-\uFA29';
  static JAAZ = '\u3000-\u303F\u3040-\u309F\u30A0-\u30FF\uFF00-\uFFEF\u4E00-\u9FAF\u2605-\u2606\u2190-\u2195\u203B';
  static ACCENTEDLETTERS = 'A-zÀ-ÖØ-öø-ÿ';
  static numbers = '0-9';
  static letters = CustomValidators.ACCENTEDLETTERS + CustomValidators.CHAZ + CustomValidators.JAAZ;
  static nameRegex = new RegExp("^([" + CustomValidators.letters + "\\s\-])*$");
  static address = CustomValidators.letters + CustomValidators.numbers;
  static alphaNum = new RegExp("^([" + CustomValidators.address + "\\s\-])*$");
  static addressRegex = new RegExp("^([" + CustomValidators.address + "#,'\\s\/-])*$");
  static dobRegex = new RegExp("^([" + CustomValidators.numbers + "\\/])*$");
  static thirtyOneDays = [1,3,5,7,8,10,12];
  emailUniqueCheck = 0;
  nicknameUniqueCheck = 0;

  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    let message;

    const password = typeof c.get('password').value == 'string' &&  c.get('password').value ? c.get('password').value.trim():c.get('password').value;
    const confirmPassword = typeof c.get('confirmPassword').value == 'string' &&  c.get('confirmPassword').value ? c.get('confirmPassword').value.trim() :c.get('confirmPassword').value;

    if (!password) {
      c.get('password').setErrors({ 'message': 'This field is required' });
      message = { 'message': 'This field is required' };
    }
    if (!confirmPassword) {
      c.get('confirmPassword').setErrors({ 'message': 'This field is required' });
      message = { 'message': 'This field is required' };
    }else if (confirmPassword && password && password !== confirmPassword) {
      c.get('confirmPassword').setErrors({ 'message': 'Password Doesn\'t Match' });
      message = { 'message': 'Password Doesn\'t Match' };
    }else{
      c.get('confirmPassword').setErrors(null);
      message = {};
    }
    return message;
  };

  dobValidator(c: AbstractControl): { invalid: boolean } {
    let message;

    let bDay = typeof c.get('bDay').value == 'string' &&  c.get('bDay').value ? c.get('bDay').value.trim() : c.get('bDay').value;
    let bMonth = typeof c.get('bMonth').value == 'string' &&  c.get('bMonth').value ? c.get('bMonth').value.trim() : c.get('bMonth').value;
    let bYear = typeof c.get('bYear').value == 'string' &&  c.get('bYear').value ? c.get('bYear').value.trim() : c.get('bYear').value;

    if(!bDay){
      c.get('bDay').setErrors({ 'message': 'Invalid Date' });
      message = { 'message': 'Invalid Date' };
    }

    if(!bMonth){
      c.get('bMonth').setErrors({ 'message': 'Invalid Month' });
      message = { 'message': 'Invalid Month' };
    }

    if(!bYear){
      c.get('bYear').setErrors({ 'message': 'Invalid Year' });
      message = { 'message': 'Invalid Year' };
    }

    if(bDay && bMonth && bYear){
      let age = new Date(bYear+'-'+bMonth+'-'+bDay);
      var ageDifMs = Date.now() - age.getTime();
      var ageDate = new Date(ageDifMs); // miliseconds from epoch
      if(bDay == 29 && bMonth == 2 && bYear % 4 != 0){
        //leap year
        c.get('bDay').setErrors({ 'message': 'Invalid Date' });
        c.get('bMonth').setErrors({ 'message': 'Invalid Date' });
        c.get('bYear').setErrors({ 'message': 'Invalid Date' });
        message = { 'message': 'Invalid Date' };
      }else if(bDay > 29 && bMonth == 2){
        c.get('bDay').setErrors({ 'message': 'Invalid Date' });
        c.get('bMonth').setErrors({ 'message': 'Invalid Date' });
        c.get('bYear').setErrors({ 'message': 'Invalid Date' });
        message = { 'message': 'Invalid Date' };
      }else if(bDay > 30 && ! _.contains(CustomValidators.thirtyOneDays,bMonth)){
        c.get('bDay').setErrors({ 'message': 'Invalid Date' });
        c.get('bMonth').setErrors({ 'message': 'Invalid Date' });
        c.get('bYear').setErrors({ 'message': 'Invalid Date' });
        message = { 'message': 'Invalid Date' };
      }else if(Math.abs(ageDate.getUTCFullYear() - 1970) < 18){
        c.get('bDay').setErrors({ 'message': 'Invalid Date' });
        c.get('bMonth').setErrors({ 'message': 'Invalid Date' });
        c.get('bYear').setErrors({ 'message': 'Invalid Date' });
        message = { 'message': 'Invalid Date' };
      }else {
        c.get('bDay').setErrors(null);
        c.get('bMonth').setErrors(null);
        c.get('bYear').setErrors(null);
        message = {};
      }
    }
    return message;
  };

  startEndDateCheck(c: AbstractControl): { invalid: boolean } {
    let message;
    const startDate = typeof c.get('startDate').value == 'string' &&  c.get('startDate').value ? c.get('startDate').value.trim() : c.get('startDate').value;
    const endDate = typeof c.get('endDate').value == 'string' &&  c.get('endDate').value ? c.get('endDate').value.trim() : c.get('endDate').value;
    if (!startDate) {
      c.get('startDate').setErrors({ 'invalid': true });
      message = { 'message': 'This field is required' };
    }
    if (!endDate) {
      c.get('endDate').setErrors({ 'invalid': true });
      message = { 'message': 'This field is required' };
    }

    if (startDate && endDate && Date.parse(endDate) < Date.parse(startDate)) {
      c.get('endDate').setErrors({ 'message': 'End Date Cannot be smaller than Start Date' });
      message = { 'message': 'End Date Cannot be smaller than Start Date' };
    }
    return message;
  };

  validName(minChar, maxChar) {
    return function (control: FormControl) {
      const name = typeof control.value == 'string' &&  control.value ?  control.value.trim() : control.value;
      let message;
      if (!name) {
        message = { 'message': 'This field is required' };
      } else if (name && name.length < minChar) {
        message = { 'message': 'This field must be minimum of ' + minChar + ' letters' };
      } else if (name && name.length > maxChar) {
        message = { 'message': 'This field cannot be more then ' + maxChar + ' letters' };
      } else if (!CustomValidators.nameRegex.test(name)) {
        message = { 'message': 'This field should contain characters only' };
      } else {
        message = null;
      }

      return message;
    };
  };

  validAlphaNumeric(minChar,maxChar) {
    return function (control: FormControl) {
      const name = typeof control.value == 'string' &&  control.value ?  control.value.trim() : control.value;
      let message;
      var regEx = /^[a-z0-9]+$/i

      if (!name) {
        message = { 'message': 'This field is required' };
      } else if (!regEx.test(name)) {
        message = { 'message': 'Invalid input' };
      }
      else if (name && name.length < minChar) {
        message = { 'message': 'This field must be minimum of ' + minChar + ' letters' };
      }else if (name && maxChar && name.length > maxChar) {
        message = { 'message': 'This field cannot exceed ' + maxChar + ' letters' };
      } else {
        message = null;
      }

      return message;
    };
  };

  reqMinMaxNum(minNum, maxChar) {
    return function (control: FormControl) {
      const value = typeof control.value == 'string' &&  control.value ? control.value.trim() : control.value;
      let message;
      var regEx = /^[0-9]*$/;
      if (!value) {
        message = { 'message': 'This field is required' };
      } else if (!regEx.test(value)) {
        message = { 'message': 'This field can contain only Numbers' };
      } else if (value.length < minNum) {
        message = { 'message': 'This field must be minimum of ' + minNum + ' letters' };
      } else if (value.length > maxChar) {
        message = { 'message': 'This field cannot be more then ' + maxChar + ' letters' };
      } else {
        message = null;
      }

      return message;
    };
  };

  validNumbers(minChar) {
    return function (control: FormControl) {
      const name = typeof control.value == 'string' &&  control.value ? control.value.trim() : control.value;
      let message;
      var regEx = /^[0-9]*$/;

      if (!name) {
        message = { 'message': 'This field is required' };
      } else if (name && !regEx.test(name)) {
        message = { 'message': 'This field can contain only Numbers' };
      } else if (name && name.length < minChar) {
        message = { 'message': 'This field must be minimum of ' + minChar + ' letters' };
      } else {
        message = null;
      }

      return message;
    };
  };

  minValueNumber(minValue,maxValue,fieldName) {
    return function (control: FormControl) {
      let name = typeof control.value == 'string' &&  control.value ?  control.value.trim() : control.value;
      let message;
      var regEx = /^[0-9]*(\.\d{0,2})?/;
      minValue = Number(minValue);
      maxValue = Number(maxValue);
      if (!name) {
        message = { 'message': 'This field is required' };
      }else{
        name  = Number(name);
        if (!name || (name && !regEx.test(name))) {
          message = { 'message': 'This field can contain only Numbers' };
        } else if (name && minValue && name < minValue) {
          message = { 'message': fieldName+' cannot be less than '+minValue };
        } else if (name && maxValue && name > maxValue) {
          message = { 'message': fieldName+' cannot be greater than '+maxValue };
        }else {
          message = null;
        }
      }
      return message;
    };
  };

  exactNumberMatch(value) {
    return function (control: FormControl) {
      const name =  typeof control.value == 'string' && control.value ?  control.value.trim() : control.value;
      let message;
      var regEx = /^[0-9]*$/;
      value = Number(value);
      if (!name) {
        message = { 'message': 'This field is required' };
      } else if (name && !regEx.test(name)) {
        message = { 'message': 'This field can contain only Numbers' };
      } else if (name && value && name.length != value) {
        message = { 'message': 'Should contain '+value+' numbers' };
      }else {
        message = null;
      }

      return message;
    };
  };

  maxLength(value) {
    return function (control: FormControl) {
      const name =typeof control.value == 'string' && control.value ? control.value.trim() : control.value;
      let message;
      value = Number(value);
      if (!name) {
        message = { 'message': 'This field is required' };
      } else if (name && value && name.length > value) {
        message = { 'message': 'Maximum of '+value+' characters allowed' };
      }else {
        message = null;
      }

      return message;
    };
  };

  phoneNumberValidator(c: FormControl): ValidationErrors {
    const phone = typeof c.value == 'string' && c.value ? c.value.trim() : c.value;
    var regEx = /^[0-9]*$/;
    var mindigit = 5;
    var maxdigit = 10;
    let message;
    if (!phone) {
      message = { 'message': 'This field is required' };
    } else if (phone && !regEx.test(phone)) {
      message = { 'message': 'Phone Number can contain only Numbers' };
    } else if (phone && phone.length < mindigit) {
      message = { 'message': 'Phone Number should be minimum of ' + mindigit + ' numbers' };
    } else if (phone && phone.length > maxdigit) {
      message = { 'message': 'Phone Number cannot be more then ' + maxdigit + ' numbers' };
    }else {
      message = null;
    }

    return message;
  };

  validateUniqueness(fieldToValidate, ajaxService: AjaxService, validateUnique) {
    let emailUniqueCheck = this.emailUniqueCheck;
    let nicknameUniqueCheck = this.nicknameUniqueCheck;
    return function (control: FormControl) {
      const name = typeof control.value == 'string' && control.value ? control.value.trim() : control.value;
      var regEx = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

      let regEx_nickname = /^[\w&.-]+$/;
      let message;
      if (!name) {
        message = { 'message': 'This field is required' };
      } else if (name && fieldToValidate == "txtEmail" && !regEx.test(name)) {
        message = { 'message': 'Email is not Valid' };
      } else if (name && fieldToValidate == "txtNickname" && name.length < 5 && !regEx.test(name)) {
        message = { 'message': 'This field must contain minimum of 5 letters' };
      } else if (name && fieldToValidate == "txtNickname" && name.length > 50 && !regEx.test(name)) {
        message = { 'message': 'This field cannot contain more than 50 letters' };
      }else if (validateUnique) {
        let validateValues = {
          [fieldToValidate]: name
        }
        if(fieldToValidate == "txtEmail") emailUniqueCheck++;
        if(fieldToValidate == "txtNickname") nicknameUniqueCheck++;

        Promise.resolve(ajaxService.validateUniqueness(validateValues))
          .then(uniqueData => {
            if(fieldToValidate == "txtEmail" && --emailUniqueCheck == 0 ){
              if (uniqueData && uniqueData["response"] == "1") {
                message = null;
                control.setErrors(null);
              } else if (uniqueData && uniqueData["response"] == "-1") {
                message = { 'message': 'Invalid Email'};
                control.setErrors({ 'message': 'Invalid Email'});
              }else {
                message = { 'message': 'Email already Registered'};
                control.setErrors(message);
              }
            }else if(fieldToValidate == "txtNickname" && --nicknameUniqueCheck == 0){
              if (uniqueData && uniqueData["response"] == "1") {
                message = null;
                control.setErrors(null);
              } else if (uniqueData && uniqueData["response"] == "-1") {
                message = { 'message': 'Invalid Username'};
                control.setErrors({ 'message': 'Invalid Username'});
              } else {
                message = { 'message': 'Username already Registered' };
                control.setErrors(message);
              }
            }

          });

      }

      return message;
    };
  };

  required(c: FormControl): ValidationErrors {
    const value = typeof c.value == 'string' && c.value ? c.value.trim() : c.value;
    let message;
    if (!value || value.length<=0) {
      message = { 'message': 'This field is required' };
    } else {
      message = null;
    }

    return message;
  }

  addressValidator(isOptional) {
    return function (c: FormControl) {
      const address = typeof c.value == 'string' &&c.value ? c.value.trim() : c.value;
      var mindigit = 2;
      let maxdigit = 200;
      let message;
      if (!address && !isOptional) {
        message = { 'message': 'This field is required' };
      }
      else if (address && address.length < mindigit) {
        message = { 'message': 'Too short address' };
      }else if (address && address.length > maxdigit) {
        message = { 'message': 'Too Long address' };
      } else if (!CustomValidators.addressRegex.test(address)) {
        message = { 'message': 'This field can have only characters and  \/\'#,-\'' };
      } else {
        message = null;
      }

      return message;
    }

  };

  reqMin(minNum) {
    return function (control: FormControl) {
      const value = typeof control.value == 'string' && control.value ? control.value.trim() : control.value;
      let message;
      if (!value) {
        message = { 'message': 'This field is required' };
      } else if (value.length < minNum) {
        message = { 'message': 'This field must be minimum of ' + minNum + ' letters' };
        control.setErrors(message);
      }
      else {
        message = null;
      }

      return message;
    };
  };

  creditCardValidator(utils: Utility) {
    return function (control: FormControl) {
      const ccNumber = typeof control.get('CreditcardNumber').value == 'string' && control.get("CreditcardNumber").value ? control.get("CreditcardNumber").value.trim() : control.get("CreditcardNumber").value;//.split('-').join('');
      const newCard = 0;//control.get("cardId").value;
      var regEx = /^[0-9]*$/;
      var mindigit = 2;
      let message;


      if (newCard == 0) {
        if (!ccNumber) {
          control.get('CreditcardNumber').setErrors({ 'message': 'This field is required' });
          message = { 'message': 'This field is required' };
        } else if (ccNumber && !regEx.test(ccNumber)) {
          control.get('CreditcardNumber').setErrors({ 'message': 'This field can contain only Numbers' });
          message = { 'message': 'This field can contain only Numbers' };
        } else if (ccNumber && !utils.creditCardValidation(ccNumber)) {
          control.get('CreditcardNumber').setErrors({ 'message': 'Please enter a valid card number' });
          message = { 'message': 'Please enter a valid card number' };
        } else {
          message = null;
        }
      } else {
        message = null;
      }


      return message;
    };
  };

  expCardData() {
    return function (control: FormControl) {
      const expiryMonth = typeof control.get('expiryMonth').value == 'string' && control.get("expiryMonth").value ? control.get("expiryMonth").value.trim() : control.get("expiryMonth").value;
      const expiryYear = typeof control.get('expiryYear').value == 'string' && control.get("expiryYear").value ? control.get("expiryYear").value.trim() : control.get("expiryYear").value;

      var regEx = /^[0-9]*$/;
      let date = new Date();
      let month = date.getMonth() + 1;
      let year = date.getFullYear();
      let message;


      // var regEx = /^[0-9]*$/;

      // if (!name) {
      //   message = { 'message': 'This field is required' };
      // } else if (name && !regEx.test(name)) {
      //   message = { 'message': 'This field can contain only Numbers' };
      // } else if (name && name.length < minChar) {
      //   message = { 'message': 'This field must be minimum of ' + minChar + ' letters' };
      // } else {
      //   message = null;
      // }


      if (!expiryMonth) {
        message = { 'message': 'This field is required' };
        control.get("expiryMonth").setErrors({ 'message': 'This field is required' });
      }else if(!regEx.test(expiryMonth)){
        message = { 'message': 'card exp ... month invalid' };
        control.get("expiryMonth").setErrors({ 'message': 'Invalid Month' });
      } else if (expiryMonth < 1 || expiryMonth > 12) {
        message = { 'message': 'card exp ... month invalid' };
        control.get("expiryMonth").setErrors({ 'message': 'Invalid Month' });
      } else if (!expiryYear) {
        message = { 'message': 'This field is required' };
        control.get("expiryYear").setErrors({ 'message': 'This field is required' });
      }else if(!regEx.test(expiryYear)){
        message = { 'message': 'card exp ... month invalid' };
        control.get("expiryYear").setErrors({ 'message': 'Invalid Year' });
      }else if (expiryYear && expiryYear.length != 4) {
        message = { 'message': 'This field must be of 4 numbers' };
        control.get("expiryYear").setErrors({ 'message': 'Must be of 4 numbers' })
      }  else if (expiryYear < year) {
        message = { 'message': 'card exp ... year invalid' };
        control.get("expiryYear").setErrors({ 'message': 'Invalid Year' })
      } else if (expiryYear == year) {
        if (expiryMonth < month) {
          message = { 'message': 'card exp ... month invalid' };
          control.get("expiryMonth").setErrors({ 'message': 'Invalid Month' })
        } else if (expiryMonth > 12) {
          message = { 'message': 'card exp ... month invalid' };
          control.get("expiryMonth").setErrors({ 'message': 'Invalid Month' });
        } else {
          control.get("expiryMonth").setErrors(null);
          control.get("expiryYear").setErrors(null);

        }
      } else if (expiryYear > year) {

        control.get("expiryMonth").setErrors(null);
        control.get("expiryYear").setErrors(null);
      }
      return message;
    }
  }

}
