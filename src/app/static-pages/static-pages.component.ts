import { Component, OnInit, ViewEncapsulation, Input, SimpleChange, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../environments/environment';
import { StaticPagesService } from './services/static-pages.service';
import { EmitterService } from '../services/emitter.service';
import { UserDetailsService } from '../services/user-details.service';
import { Utility } from '../utility/utility';
import * as $ from 'jquery';

@Component({
  selector: 'app-static-pages',
  templateUrl: './static-pages.component.html',
  styleUrls: ['./static-pages.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StaticPagesComponent implements OnInit {
  @Input() url;
  @Input() callingFrom;
  @Output() staticPageAction = new EventEmitter<any>();
  isPageAvailable;
  loader = true;
  faqCategories;
  faqQA;
  leftMenuOpenend;
  pageUrl;

  constructor(
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private staticPagesService:StaticPagesService,
    private emitterService:EmitterService,
    private userDetailsService:UserDetailsService,
    private utils:Utility
  ) { }

  ngOnInit() {
    if(this.callingFrom != 'popup' && this.callingFrom != 'sideMenu'){
      this.activatedRoute.params.subscribe(
        params =>{
          if(params["url"])
            this.getStaticPageData(params["url"]);
        }
      )
    }

    

  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange
  }): void {
    changes['url'] ? this.url = changes['url'].currentValue : '';
    if(this.url) this.getStaticPageData(this.url)
  }

  getStaticPageData(url){
    let self = this;
    self.pageUrl = url;
    this.loader = true;
    window["prerenderReady"] = false;
    Promise.resolve(this.staticPagesService.getContent({'lang':'en','url_path':url+"-jkr"}))
    .then(
      staticPageData=>{
        this.loader = false
        if(staticPageData["url_path_error"] || !staticPageData["content"]){
          this.isPageAvailable = false;
        }else{
          let getUserBrowserCountry = this.userDetailsService.getUserBrowserCountry()
          this.isPageAvailable = true;
          let staticContent = staticPageData["content"].replace(new RegExp('{{site_url}}','g'), environment.siteUrl);
          let staticHead = staticPageData["head"].replace(new RegExp('{{site_url}}','g'), environment.siteUrl);
          staticContent = staticContent.replace(new RegExp('{{country_code}}','g'), this.userDetailsService.getUserBrowserCountry());
          staticHead = staticHead.replace(new RegExp('{{site_url}}','g'), environment.siteUrl);
          setTimeout(function(){
            $("#staticPageHead"+(self.callingFrom ? self.callingFrom : '')).html(staticHead);
            $("#staticPageContent"+(self.callingFrom ? self.callingFrom : '')).html(staticContent);
            if(url == "faq"){
              self.getFaqCategoriesAndQA();
            }else{
              this.faqCategories = undefined;
              this.faqQA = undefined;
            }
            if(url == "terms-and-conditions" || url == "promo-terms"){
              if(getUserBrowserCountry == "DE" || getUserBrowserCountry == "UK" || getUserBrowserCountry == "NL"){
                $("#tnc-country").removeClass('hide');
              }else{
                $("#tnc-country").addClass('hide');
              }

            }
            if(url == "hamburger-menu") self.menuExpand();
            if(self.callingFrom != 'sideMenu') self.utils.setSEO(staticPageData,false);
            self.utils.elementsChange();
            window["prerenderReady"] = true;
            $("#staticPageContent"+(self.callingFrom ? self.callingFrom : '')).on('click',function(e){
              let routerLink = $(e.target).attr("routerLink");
              if(routerLink){
                self.utils.cmsRouter(routerLink);
              }
            })
          })
        }

      }
    )
  }

  getFaqCategoriesAndQA(){
    Promise.resolve(this.staticPagesService.getFaqQuestionsAndCategory())
    .then(
      faqCatQAResponse => {
        if(faqCatQAResponse){
          this.faqCategories = faqCatQAResponse["faq_categories"];
          this.faqQA = faqCatQAResponse["faq_question"];
        }
      }
    )
  }



  checkForExpand(e){
    this.leftMenuOpenend = this.utils.checkForExpand(this.leftMenuOpenend,e)
  }

  menuExpand(){
    var self = this;
    $('.expandable-menu').on('click',function(e){
      self.checkForExpand(e);
    })

    $('.icon-plus','.icon-minus').on('click',function(e){
      self.checkForExpand(e);
    })
  }



  ngOnDestroy(){
    this.utils.setSEO("","");
  }

}
