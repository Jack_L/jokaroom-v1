import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StaticPagesComponent } from './static-pages.component';
import { SharedModule } from '../shared-modules/shared.module';
import { StaticPagesPopupComponent } from './static-pages-popup/static-pages-popup.component';
import { StaticPagesService } from './services/static-pages.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule.forRoot()
  ],
  declarations: [
    StaticPagesComponent,
    StaticPagesPopupComponent
  ],
  exports:[
    StaticPagesPopupComponent,
    StaticPagesComponent
  ],
  providers:[
    StaticPagesService
  ]
})
export class StaticPagesModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: StaticPagesModule,
      providers: []
    };
  }
}
