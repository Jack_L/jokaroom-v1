import { Injectable } from '@angular/core';
import { AjaxService } from '../../services/ajax.service';

@Injectable()
export class StaticPagesService {

  constructor(
    private ajaxService:AjaxService
  ) { }

  getContent(staticPageData){
    return Promise.resolve(this.ajaxService.getStaticPage(staticPageData))
    .then(
      staticPageResponse =>{
        return staticPageResponse;
      }
    )
  }

  getFaqQuestionsAndCategory(){
    return Promise.resolve(this.ajaxService.getFaqQuestionsAndCategory())
    .then(
      faqCatQAResponse =>{
        return faqCatQAResponse;
      }
    )
  }

}
