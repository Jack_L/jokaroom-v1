import { Component, OnInit, Input, SimpleChange } from '@angular/core';

@Component({
  selector: 'app-country-block',
  templateUrl: './country-block.component.html',
  styleUrls: ['./country-block.component.scss']
})
export class CountryBlockComponent implements OnInit {
  @Input() countryCode;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange
  }): void {
    changes['countryCode'] ? this.countryCode = changes['countryCode'].currentValue : '';
  }

}
