import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { PromotionsComponent } from '../promotions/promotions.component'
import { PromotionContentComponent } from '../promotions/promotion-content/promotion-content.component';
import { StaticPagesComponent } from '../static-pages/static-pages.component';


@NgModule({
  imports: [
    RouterModule.forRoot([{
        path: '',
        component: HomeComponent
      },
      {
        path: ':lang',
        component: HomeComponent
      },
      {
        path: ':lang/gamePlay/:gameId',
        loadChildren: "../game-play/game-play.module#GamePlayModule"
      },
      {
        path: ':lang/promotions',
        component: PromotionsComponent
      },
      {
        path: ':lang/promotions/:promoUrl',
        component: PromotionContentComponent
      },
      {
        path: ':lang/:url',
        component: StaticPagesComponent
      },
      {
        path: '**',
        component: PageNotFoundComponent
      }

    ], { useHash: false })
  ],
  exports: [RouterModule]
})
export class AppRouterModule {}

/*
,
{
  path: 'banking',
  component: bankingComponent
},
{
  path: 'about-us',
  component: aboutUsComponent
},
{
  path: 'mobile',
  component: mobileComponent
},
{
  path: 'contact-us',
  component: contactUsComponent
},
{
  path: 'terms-and-conditions',
  component: TermsAndConditionsComponent
},
{
  path: 'promo-terms',
  component: PromotionalTermsAndConditionsComponent
},
{
  path: 'faq',
  component: FaqComponent
},
{
  path: 'club-mate',
  component: clubMateComponent,
  pathMatch: 'prefix'
}
*/
