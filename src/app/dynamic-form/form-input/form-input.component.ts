import { Component,OnInit, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Utility } from '../../utility/utility';
import { FormValidationComponent } from '../../shared-modules/form-validation/form-validation.component';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FormInputComponent extends FormValidationComponent implements OnInit {
  config;
  group: FormGroup;
  selectedMethodData: any;
  callingFrom: any;
  focusedElement;
  usedAccount:boolean;
  constructor(
    private utils:Utility
  ) {
    super(utils);
   }

  ngOnInit() {
  }

}
