import { Component, OnInit,Output,EventEmitter, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Utility } from '../../utility/utility';
import { FormValidationComponent } from '../../shared-modules/form-validation/form-validation.component';
@Component({
  selector: 'app-form-button',
  templateUrl: './form-button.component.html',
  styleUrls: ['./form-button.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FormButtonComponent extends FormValidationComponent implements OnInit {
  config;
  group: FormGroup;
  selectedMethodData: any;
  callingFrom: any;
  usedAccount:boolean;
  constructor(
    private utils:Utility
  ) {
    super(utils);
  }

  ngOnInit() {
  }

  @Output() buttonClicked = new EventEmitter<string>();

  clickButton(message) {
   this.buttonClicked.emit(message);
 }

}
