import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Utility } from '../../utility/utility';
import { FormValidationComponent } from '../../shared-modules/form-validation/form-validation.component';

@Component({
  selector: 'app-form-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.css']
})
export class FormSelectComponent extends FormValidationComponent implements OnInit {
  config;
  group: FormGroup;
  selectedMethodData: any;
  callingFrom: any;
  usedAccount:boolean;
  constructor(
    private utils:Utility
  ) {
    super(utils);
  }

  ngOnInit() {
  }

}
