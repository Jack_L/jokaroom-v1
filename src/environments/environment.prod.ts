export const environment = {
  production: true,
  siteUrl:"https://prelive.jokaroom.com",
  apiUrl:"https://service.jokaroom.com",
  ldApiUrl: "https://apiStage.lottoDay.com",
  websiteCode: "JKR",
  paymentIqUrl:"https://api.paymentiq.io/paymentiq",
  paymentIqMID:"100070003",
  paymentIqUID:"TEST_EUR",
  paymentIqRedirectSuccess:"http://local.joka-room.com:8085/assets/redirect.html",
  paymentIqRedirectFail:"http://local.joka-room.com:8085/assets/redirect.html",
  paymentIqRedirectPending:"http://local.joka-room.com:8085/assets/redirect.html"
};
