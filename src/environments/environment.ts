// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  siteUrl:"http://stage-jokaroom.crm-secure.com:8081",
  apiUrl:"https://stage-jokaroom.crm-secure.com:8082",
  ldApiUrl: "https://apiStage.lottoDay.com",
  websiteCode: "JKR",
  paymentIqUrl:"https://test-api.paymentiq.io/paymentiq",
  paymentIqMID:"100118998",
  paymentIqUID:"jokaroom",
  paymentIqRedirectSuccess:"http://local.joka-room.com:8085/assets/redirect.html",
  paymentIqRedirectFail:"http://local.joka-room.com:8085/assets/redirect.html",
  paymentIqRedirectPending:"http://local.joka-room.com:8085/assets/redirect.html"
};
